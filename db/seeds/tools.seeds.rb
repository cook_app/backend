p('=== Seed tools... ===')
p('- Tool')

Tool.tool_types.values.each do |tool_type|
  Tool.find_or_create_by!(tool_type: tool_type, brand: nil, tool_model: nil)
end
