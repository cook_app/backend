p('=== Seed development:ingredients... ===')
p('- Ingredient::Category')
p('- Ingredient')

ingredients_data = YAML.load_file("#{Rails.root}/db/seeds/development/ingredients.seeds.yml")

ingredients_data['ingredient_categories'].each do |ingredient_category_data|
  ingredient_category = Ingredient::Category.find_or_initialize_by(name: ingredient_category_data['name'])

  unless ingredient_category.persisted?
    if ingredient_category_data['image_name'].present?
      ingredient_category.image = File.open("#{Rails.root}/db/seeds/development/images/ingredient_categories/#{ingredient_category_data['image_name']}")
    end
    ingredient_category.save!
  end

  ingredient_category_data['ingredients'].each do |ingredient_data|
    ingredient = Ingredient.find_or_initialize_by(name: ingredient_data['name'])
    next if ingredient.persisted?

    if ingredient_data['image_name'].present?
      ingredient.image = File.open("#{Rails.root}/db/seeds/development/images/ingredients/#{ingredient_data['image_name']}")
    end
    ingredient.ingredient_category = ingredient_category
    ingredient.unit = ingredient_data['unit']
    ingredient.save!
  end
end
