p('=== Seed development:tools... ===')
p('- Tool')

tools_data = YAML.load_file("#{Rails.root}/db/seeds/development/tools.seeds.yml")

tools_data['tools'].each do |tool_attributes|
  Tool.find_or_create_by!(tool_attributes)
end
