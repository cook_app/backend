p('=== Seed development:admin_users... ===')
p('- AdminUser')

admin_user = AdminUser.find_by(email: 'admin@example.com')
if admin_user.nil?
  AdminUser.create!(
    email: 'admin@example.com',
    encrypted_password: AdminUsers::PasswordService.new.encrypt('Qqwerty123'),
  )
end
