after 'development:users', 'development:recipes' do
  p('=== Seed development:marks... ===')
  p('- Mark')

  recipes = Recipe.all[0..3]

  user1 = User.find_by(email: 'test1@example.com')
  Mark.find_by(recipe: recipes.first) || Mark.create!(user: user1, recipe: recipes.first, value: 4)
  Mark.find_by(recipe: recipes.second) || Mark.create!(user: user1, recipe: recipes.second, value: 2)
  Mark.find_by(recipe: recipes.fourth) || Mark.create!(user: user1, recipe: recipes.fourth, value: 5)

  user2 = User.find_by(email: 'test2@example.com')
  Mark.find_by(recipe: recipes.first) || Mark.create!(user: user2, recipe: recipes.first, value: 5)
  Mark.find_by(recipe: recipes.second) || Mark.create!(user: user2, recipe: recipes.second, value: 1)
  Mark.find_by(recipe: recipes.third) || Mark.create!(user: user2, recipe: recipes.third, value: 3)

  user3 = User.find_by(email: 'test3@example.com')
  Mark.find_by(recipe: recipes.first) || Mark.create!(user: user3, recipe: recipes.first, value: 4)
  Mark.find_by(recipe: recipes.second) || Mark.create!(user: user3, recipe: recipes.second, value: 3)
end
