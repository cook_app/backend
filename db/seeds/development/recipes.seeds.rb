after 'development:tools', 'development:ingredients' do
  p('=== Seed development:recipes... ===')
  p('- Recipe::Category')
  p('- Recipe')
  p('- Step')
  p('- Recipe::Ingredient')

  recipes_data = YAML.load_file("#{Rails.root}/db/seeds/development/recipes.seeds.yml")

  recipes_data['recipe_categories'].each do |recipe_category_data|
    # === Recipe::Category

    recipe_category = Recipe::Category.find_or_initialize_by(name: recipe_category_data['name'])

    unless recipe_category.persisted?
      if recipe_category_data['image_name'].present?
        recipe_category.image = File.open("#{Rails.root}/db/seeds/development/images/recipe_categories/#{recipe_category_data['image_name']}")
      end
      recipe_category.save!
    end

    recipe_category_data['recipes'].each do |recipe_data|

      # === Recipe

      recipe = Recipe.find_or_initialize_by(name: recipe_data['name'])

      unless recipe.persisted?
        if recipe_data['image_name'].present?
          recipe.image = File.open("#{Rails.root}/db/seeds/development/images/recipes/#{recipe_data['image_name']}")
        end
        recipe.recipe_category = recipe_category
        recipe.assign_attributes(
          description: recipe_data['description'],
          kilocalories: rand(500..1000),
          fats: rand(20..30),
          carbohydrates: rand(50..70),
          proteins: rand(15..25),
        )
        recipe.save!
      end

      # === Step

      if recipe.steps.count.zero?

        step_attributes_proc = -> (recipe, time, action) {
          tools = if action == 'Пожарить на сковороде на сильном огне'
            [Tool.find_by(tool_type: 'frying_pan')]
          elsif ['Нарезать кубиками овощи', 'Нарезать кубиками фрукты'].include?(action)
            [
              Tool.where(tool_type: 'knife').sample,
              Tool.find_by(tool_type: 'cutting_board'),
            ]
          else
            []
          end
          tools.compact!

          {
            name: "Шаг #{time}",
            content: action,
            duration_seconds: rand(40..600),
            position: time,
            recipe: recipe,
            image: rand(0..4).zero? ? File.open("#{Rails.root}/db/seeds/development/images/steps/#{['pot.jpeg', 'frying_pan.jpeg'].sample}") : nil,
            tools: tools,
          }
        }

        actions = [
          'Пожарить на сковороде на сильном огне',
          'Нарезать кубиками овощи',
          'Нарезать кубиками фрукты',
          'Залить маслом и перемешать',
          'Варить на медленном огне в кастрюле с накрытой крышкой',
          'Добавить щепотку специй и тщательно перемешать',
          'Тщательно перемешать ингредиенты',
          'Нарезать на равные куски и подать к столу',
        ]

        (1..rand(2..5)).to_a.map do |step_time|
          step_attributes_proc.call(recipe, step_time, actions.delete(actions.sample))
        end.each do |step_attributes|
          Step.create!(step_attributes)
        end
      end

      # === Recipe::Ingredient

      if recipe.ingredients.blank?
        recipe_data['ingredient_names'].each do |ingredient_name|
          Recipe::Ingredient.create!(
            recipe: recipe,
            ingredient: Ingredient.find_by!(name: ingredient_name),
            amount: rand(20..200),
          )
        end
      end
    end
  end
end
