p('=== Seed development:users... ===')
p('- User')

users_data = YAML.load_file("#{Rails.root}/db/seeds/development/users.seeds.yml")

users_data['users'].each do |user_attributes|
  user = User.find_or_initialize_by(email: user_attributes['email'])
  next if user.persisted?

  user.name = user_attributes['name']
  if user_attributes['avatar_file_name'].present?
    user.avatar = File.open("#{Rails.root}/db/seeds/development/images/users/#{user_attributes['avatar_file_name']}")
  end

  user.save!
end
