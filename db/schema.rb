# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_13_152401) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource"
  end

  create_table "admin_users", comment: "Пользователи, которые имеют доступ в панель админа", force: :cascade do |t|
    t.string "email", null: false, comment: "Почта пользователя"
    t.string "encrypted_password", null: false, comment: "Зашифрованный пароль пользователя"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
  end

  create_table "auth_refresh_tokens", comment: "Токены обновления в рамках авторизации", force: :cascade do |t|
    t.uuid "user_id", comment: "Пользователь которому принадлежит токен обновления"
    t.string "encrypted_refresh_token", null: false, comment: "Зашифрованный токен с помощью которого можно создать новый JWT"
    t.datetime "expires_at", null: false, comment: "Дата когда токен обновления будет невалидный"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_auth_refresh_tokens_on_user_id"
  end

  create_table "favorite_recipes", comment: "Рецепты, которые пользователь добавил в избранное", force: :cascade do |t|
    t.uuid "user_id", comment: "Пользователь"
    t.bigint "recipe_id", comment: "Рецепт"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["recipe_id"], name: "index_favorite_recipes_on_recipe_id"
    t.index ["user_id", "recipe_id"], name: "index_favorite_recipes_on_user_id_and_recipe_id", unique: true
    t.index ["user_id"], name: "index_favorite_recipes_on_user_id"
  end

  create_table "ingredient_categories", comment: "Категория ингредиентов", force: :cascade do |t|
    t.string "name", limit: 512, null: false, comment: "Название категории"
    t.jsonb "image_data", comment: "Изображение категории, служебное поле для shrine"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ingredients", comment: "Ингредиенты", force: :cascade do |t|
    t.bigint "ingredient_category_id"
    t.string "name", limit: 512, null: false, comment: "Название ингредиента"
    t.string "description", comment: "Описание ингредиента"
    t.string "unit", null: false, comment: "Единица измерения, см. модель"
    t.jsonb "image_data", comment: "Изображение ингредиента, служебное поле для shrine"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ingredient_category_id"], name: "index_ingredients_on_ingredient_category_id"
  end

  create_table "marks", comment: "Оценки рецептов пользователями", force: :cascade do |t|
    t.bigint "recipe_id", comment: "Рецепт"
    t.uuid "user_id", comment: "Пользователь"
    t.integer "value", null: false, comment: "Оценка"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["recipe_id", "user_id"], name: "index_marks_on_recipe_id_and_user_id", unique: true
    t.index ["recipe_id"], name: "index_marks_on_recipe_id"
    t.index ["user_id"], name: "index_marks_on_user_id"
  end

  create_table "oauth_identifiers", force: :cascade do |t|
    t.uuid "user_id"
    t.string "provider", null: false, comment: "Сервис через который пользователь авторизовался, поле enum, возможные варивнты см. в моделе"
    t.string "provider_user_id", null: false, comment: "Идентификатор пользователя в сервисе-провайдере"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_oauth_identifiers_on_user_id"
  end

  create_table "recipe_categories", comment: "Категории рецептов", force: :cascade do |t|
    t.string "name", limit: 512, null: false, comment: "Название"
    t.jsonb "image_data", comment: "Изображение категории, служебное поле для shrine"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "recipe_ingredients", comment: "Связующая таблица между рецептами и ингредиентами", force: :cascade do |t|
    t.bigint "recipe_id"
    t.bigint "ingredient_id"
    t.integer "amount", null: false, comment: "Количество ингредиентов, единица измерения в таблице ingredients"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ingredient_id"], name: "index_recipe_ingredients_on_ingredient_id"
    t.index ["recipe_id", "ingredient_id"], name: "index_recipe_ingredients_on_recipe_id_and_ingredient_id", unique: true
    t.index ["recipe_id"], name: "index_recipe_ingredients_on_recipe_id"
  end

  create_table "recipes", comment: "Рецепты", force: :cascade do |t|
    t.bigint "recipe_category_id", comment: "Категория"
    t.string "name", limit: 512, null: false, comment: "Название рецепта"
    t.text "description", comment: "Описание рецепта"
    t.jsonb "image_data", comment: "Изображение рецепта, служебное поле для shrine"
    t.integer "kilocalories", null: false, comment: "Килокалории"
    t.integer "fats", null: false, comment: "Жиры"
    t.integer "carbohydrates", null: false, comment: "Углеводы"
    t.integer "proteins", null: false, comment: "Белки"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["recipe_category_id"], name: "index_recipes_on_recipe_category_id"
  end

  create_table "step_tools", comment: "Связующая таблица has_and_belongs_to_many между steps и tools", force: :cascade do |t|
    t.bigint "step_id"
    t.bigint "tool_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["step_id"], name: "index_step_tools_on_step_id"
    t.index ["tool_id"], name: "index_step_tools_on_tool_id"
  end

  create_table "steps", comment: "Шаги приготовления рецептов", force: :cascade do |t|
    t.bigint "recipe_id", comment: "Рецепт"
    t.string "name", limit: 512, comment: "Название шага"
    t.text "content", null: false, comment: "Описание шага"
    t.integer "duration_seconds", comment: "Длительность выполнения шага в секундах"
    t.integer "position", null: false, comment: "Порядковый номер шага"
    t.jsonb "image_data", comment: "Изображение шага, служебное поле для shrine"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["recipe_id"], name: "index_steps_on_recipe_id"
  end

  create_table "tools", comment: "Инструменты и техника для готовки, например ножи, сковороды, плиты и т.д.", force: :cascade do |t|
    t.string "tool_type", null: false, comment: "Тип инструмента, поле enum"
    t.string "brand", comment: "Бренд инструмента"
    t.string "tool_model", comment: "Модель инструмента"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tool_type", "brand", "tool_model"], name: "index_tools_on_tool_type_and_brand_and_tool_model", unique: true
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, comment: "Пользователи", force: :cascade do |t|
    t.string "email", null: false, comment: "Email пользователя"
    t.string "name", null: false, comment: "Имя пользователя"
    t.jsonb "avatar_data", comment: "Аватар пользователя, служебное поле для shrine"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "auth_refresh_tokens", "users"
  add_foreign_key "favorite_recipes", "recipes"
  add_foreign_key "favorite_recipes", "users"
  add_foreign_key "ingredients", "ingredient_categories"
  add_foreign_key "marks", "recipes"
  add_foreign_key "marks", "users"
  add_foreign_key "oauth_identifiers", "users"
  add_foreign_key "recipe_ingredients", "ingredients"
  add_foreign_key "recipe_ingredients", "recipes"
  add_foreign_key "recipes", "recipe_categories"
  add_foreign_key "step_tools", "steps"
  add_foreign_key "step_tools", "tools"
  add_foreign_key "steps", "recipes"
end
