class CreateAuthRefreshTokens < ActiveRecord::Migration[6.1]
  def change
    create_table :auth_refresh_tokens, comment: 'Токены обновления в рамках авторизации' do |t|
      t.references :user, index: true, foreign_key: true, type: :uuid, comment: 'Пользователь которому принадлежит токен обновления'

      t.string :encrypted_refresh_token, null: false, comment: 'Зашифрованный токен с помощью которого можно создать новый JWT'
      t.datetime :expires_at, null: false, comment: 'Дата когда токен обновления будет невалидный'

      t.timestamps
    end
  end
end
