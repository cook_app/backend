class CreateFavoriteRecipes < ActiveRecord::Migration[6.1]
  def change
    create_table :favorite_recipes, comment: 'Рецепты, которые пользователь добавил в избранное' do |t|
      t.references :user, index: true, foreign_key: true, type: :uuid, comment: 'Пользователь'
      t.references :recipe, index: true, foreign_key: true, comment: 'Рецепт'

      t.timestamps

      t.index %i[user_id recipe_id], unique: true
    end
  end
end
