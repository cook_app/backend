class CreateSteps < ActiveRecord::Migration[6.1]
  def change
    create_table :steps, comment: 'Шаги приготовления рецептов' do |t|
      t.references :recipe, foreign_key: true, index: true, comment: 'Рецепт'

      t.string :name, limit: 512, comment: 'Название шага'
      t.text :content, null: false, comment: 'Описание шага'
      t.integer :duration_seconds, comment: 'Длительность выполнения шага в секундах'
      t.integer :position, null: false, comment: 'Порядковый номер шага'
      t.jsonb :image_data, comment: 'Изображение шага, служебное поле для shrine'

      t.timestamps
    end
  end
end
