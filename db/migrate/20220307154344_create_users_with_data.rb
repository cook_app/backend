class CreateUsersWithData < ActiveRecord::Migration[6.1]
  def change
    create_table :users, id: :uuid, comment: 'Пользователи' do |t|
      t.string :email, null: false, comment: 'Email пользователя'
      t.string :name, null: false, comment: 'Имя пользователя'
      t.jsonb :avatar_data, comment: 'Аватар пользователя, служебное поле для shrine'

      t.timestamps

      t.index :email, unique: true
    end
  end
end
