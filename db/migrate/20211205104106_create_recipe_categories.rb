class CreateRecipeCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :recipe_categories, comment: 'Категории рецептов' do |t|
      t.string :name, limit: 512, null: false, comment: 'Название'
      t.jsonb :image_data, comment: 'Изображение категории, служебное поле для shrine'

      t.timestamps
    end
  end
end
