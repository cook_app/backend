class CreateIngredientCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :ingredient_categories, comment: 'Категория ингредиентов' do |t|
      t.string :name, limit: 512, null: false, comment: 'Название категории'
      t.jsonb :image_data, comment: 'Изображение категории, служебное поле для shrine'

      t.timestamps
    end
  end
end
