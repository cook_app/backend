class CreateOauthIdentifiers < ActiveRecord::Migration[6.1]
  def change
    create_table :oauth_identifiers do |t|
      t.references :user, index: true, foreign_key: true, type: :uuid

      t.string :provider, null: false, comment: 'Сервис через который пользователь авторизовался, поле enum, возможные варивнты см. в моделе'
      t.string :oauth_id, null: false, comment: 'Идентификатор пользователя в сервисе-провайдере'
      t.jsonb :oauth_hash, null: false, default: {}, comment: 'Данные которые пришли из сервиса аутентификации (провайдера)'

      t.timestamps
    end
  end
end
