class CreateTools < ActiveRecord::Migration[6.1]
  def change
    create_table :tools, comment: 'Инструменты и техника для готовки, например ножи, сковороды, плиты и т.д.' do |t|
      t.string :tool_type, null: false, comment: 'Тип инструмента, поле enum'
      t.string :brand, comment: 'Бренд инструмента'
      t.string :tool_model, comment: 'Модель инструмента'

      t.timestamps

      t.index %i[tool_type brand tool_model], unique: true
    end
  end
end
