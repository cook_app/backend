class CreateRecipes < ActiveRecord::Migration[6.1]
  def change
    create_table :recipes, comment: 'Рецепты' do |t|
      t.references :recipe_category, foreign_key: true, index: true, comment: 'Категория'

      t.string :name, null: false, limit: 512, comment: 'Название рецепта'
      t.text :description, comment: 'Описание рецепта'
      t.jsonb :image_data, comment: 'Изображение рецепта, служебное поле для shrine'

      t.integer :kilocalories, null: false, comment: 'Килокалории'
      t.integer :fats, null: false, comment: 'Жиры'
      t.integer :carbohydrates, null: false, comment: 'Углеводы'
      t.integer :proteins, null: false, comment: 'Белки'

      t.timestamps
    end
  end
end
