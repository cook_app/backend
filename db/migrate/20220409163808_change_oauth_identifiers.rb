class ChangeOauthIdentifiers < ActiveRecord::Migration[6.1]
  def change
    rename_column :oauth_identifiers, :oauth_id, :provider_user_id
    rename_column :oauth_identifiers, :oauth_hash, :payload
  end
end
