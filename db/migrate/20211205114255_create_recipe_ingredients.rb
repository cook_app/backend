class CreateRecipeIngredients < ActiveRecord::Migration[6.1]
  def change
    create_table :recipe_ingredients, comment: 'Связующая таблица между рецептами и ингредиентами' do |t|
      t.references :recipe, foreign_key: true, index: true
      t.references :ingredient, foreign_key: true, index: true

      t.integer :amount, null: false, comment: 'Количество ингредиентов, единица измерения в таблице ingredients'

      t.timestamps

      t.index [:recipe_id, :ingredient_id], unique: true
    end
  end
end
