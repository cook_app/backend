class RemovePayloadFromOauthIdentifiers < ActiveRecord::Migration[6.1]
  def self.up
    remove_column :oauth_identifiers, :payload
  end

  # Специально не делаю null: false и default: {}
  def self.down
    add_column :oauth_identifiers, :payload, :jsonb, comment: 'Данные которые пришли из сервиса аутентификации (провайдера)'
  end
end
