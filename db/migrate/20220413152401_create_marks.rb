class CreateMarks < ActiveRecord::Migration[6.1]
  def change
    create_table :marks, comment: 'Оценки рецептов пользователями' do |t|
      t.references :recipe, foreign_key: true, index: true, comment: 'Рецепт'
      t.references :user, foreign_key: true, index: true, type: :uuid, comment: 'Пользователь'

      t.integer :value, null: false, comment: 'Оценка'

      t.timestamps

      t.index %i[recipe_id user_id], unique: true
    end
  end
end
