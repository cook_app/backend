class DropTableUsers < ActiveRecord::Migration[6.1]
  def up
    drop_table :users
  end

  def down
    create_table :users, comment: 'Пользователи' do |t|

      t.timestamps
    end
  end
end
