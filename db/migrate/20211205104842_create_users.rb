class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users, comment: 'Пользователи' do |t|

      t.timestamps
    end
  end
end
