class CreateStepTools < ActiveRecord::Migration[6.1]
  def change
    create_table :step_tools, comment: 'Связующая таблица has_and_belongs_to_many между steps и tools' do |t|
      t.references :step, foreign_key: true, index: true
      t.references :tool, foreign_key: true, index: true

      t.timestamps
    end
  end
end
