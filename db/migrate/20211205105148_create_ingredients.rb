class CreateIngredients < ActiveRecord::Migration[6.1]
  def change
    create_table :ingredients, comment: 'Ингредиенты' do |t|
      t.references :ingredient_category, foreign_key: true, index: true

      t.string :name, limit: 512, null: false, comment: 'Название ингредиента'
      t.string :description, comment: 'Описание ингредиента'
      t.string :unit, null: false, comment: 'Единица измерения, см. модель'
      t.jsonb :image_data, comment: 'Изображение ингредиента, служебное поле для shrine'

      t.timestamps
    end
  end
end
