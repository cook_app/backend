class ApplicationController < ActionController::Base
  protected

  # For active admin
  def authenticate_admin_user!
    return if request.path.starts_with?('/admin/session')
    return if session[:admin_user_id].present?

    redirect_to(admin_session_path)
  end

  # For active admin
  def current_admin_user
    admin_user_id = session[:admin_user_id]
    return if admin_user_id.nil?

    AdminUser.find(admin_user_id)
  end
end
