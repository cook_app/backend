class OauthController < ActionController::Base
  skip_before_action :verify_authenticity_token, only: :auth

  def auth
    provider = params['provider']
    oauth_factory_auth_service_result = Oauth::FactoryAuthService.new.call(provider: provider)
    if oauth_factory_auth_service_result.failure?
      return render(
        json: { errors: [oauth_factory_auth_service_result.data[:error]] },
        status: :bad_request,
      )
    end

    auth_service = oauth_factory_auth_service_result.data[:auth_service]
    auth_service_result = auth_service.call(params: params)
    if auth_service_result.failure?
      return render(
        json: { errors: [auth_service_result.data[:error]] },
        status: :bad_request,
      )
    end

    token_pair_generation_result = Auth::TokenPairService.new.generate_and_save(user: auth_service_result.data[:user])
    if token_pair_generation_result.failure?
      return render(
        json: { errors: [token_pair_generation_result.data[:error]] },
        status: :internal_server_error,
      )
    end

    render(
      json: {
        jwt: token_pair_generation_result.data[:jwt], 
        refresh_token: token_pair_generation_result.data[:refresh_token], 
      }
    )
  end
end
