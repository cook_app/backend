# frozen_string_literal: true

module Api
  module V1
    class IngredientsController < ApplicationController
      INGREDIENT_RECIPES_LIMIT = 10

      def show
        @ingredient = Ingredient.find(params[:id])
      end

      def autocomplete_search
        elastic_ingredients_query = ElasticQueryBuilders::IngredientQueryBuilder.new.build(
          **autocomplete_search_params_hash.slice(:full_text_value)
        )

        elastic_pagination = ElasticPagination.new(
          page: autocomplete_search_params_hash[:page],
          size: autocomplete_search_params_hash[:items_count]
        )
        elastic_ingredients_query = elastic_pagination.build_query(query: elastic_ingredients_query)

        elastic_ingredients_query_result = Ingredient.__elasticsearch__.search(elastic_ingredients_query)

        @elastic_ingredients = elastic_ingredients_query_result.to_a
        @pagination_options = elastic_pagination.pagination_options(
          query_response: elastic_ingredients_query_result.response
        )
      end

      private

      def autocomplete_search_params_hash
        result = params.permit(:full_text_value, :page, :items_count).to_h.symbolize_keys
        raise MissingParamsError.new(%i[full_text_value]) if result[:full_text_value].blank?

        result[:page] = result[:page].to_i if result[:page].present?
        result[:items_count] = result[:items_count].to_i if result[:items_count].present?

        result
      end
    end
  end
end
