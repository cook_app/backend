module Api
  module V1
    class ToolsController < ApplicationController
      def index
        tools = Tools::IndexQuery.new.call(**index_params_hash)
        @pagy, paginated_tools = paginate_by_pagy(tools)
        @decorated_tools = tools.decorate
      end

      private

      def index_params_hash
        result = params.permit(:only_unique_tool_types).to_h.symbolize_keys
        if result[:only_unique_tool_types].present?
          result[:only_unique_tool_types] = ActiveModel::Type::Boolean.new.cast(result[:only_unique_tool_types])
        end
        result
      end
    end
  end
end
