module Api
  module V1
    class MarksController < ApplicationController
      def create_or_update
        @mark = Mark.find_or_initialize_by(user: current_user, recipe_id: params[:recipe_id])
        authorize(@mark, :update?)

        @mark.value = params[:value]

        if @mark.save
          @recipe_rating = Recipes::RatingService.new.calculate(marks: @mark.recipe.marks)
        else
          render_error(
            I18n.t(
              :record_invalid,
              scope: :api_errors,
              class_name: @mark.class.name,
              full_messages: @mark.errors.full_messages.join(', ')
            ),
            :bad_request
          )
        end
      end

      def destroy
        mark = Mark.find_by(id: params[:id])
        return head(:ok) if mark.nil?

        authorize(mark)

        if mark.destroy
          @recipe_rating = Recipes::RatingService.new.calculate(marks: mark.recipe.marks)
        else
          render_error(
            I18n.t(
              :record_destroying_failed,
              scope: :api_errors,
              class_name: mark.class.name,
              full_messages: mark.errors.full_messages.join(', ')
            ),
            :bad_request
          )
        end
      end
    end
  end
end
