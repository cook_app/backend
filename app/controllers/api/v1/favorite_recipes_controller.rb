module Api
  module V1
    class FavoriteRecipesController < ApplicationController
      def create_or_destroy
        favorite_recipe = FavoriteRecipe.find_or_initialize_by(user: current_user, recipe_id: params[:recipe_id])

        if favorite_recipe.persisted?
          if favorite_recipe.destroy
            head(:ok)
          else
            render_error(
              I18n.t(
                :record_action_error,
                scope: :api_errors,
                class_name: favorite_recipe.class.name,
                full_messages: favorite_recipe.errors.full_messages.join(', ')
              ),
              :bad_request
            )
          end
        else
          if favorite_recipe.save
            head(:ok)
          else
            render_error(
              I18n.t(
                :record_action_error,
                scope: :api_errors,
                class_name: favorite_recipe.class.name,
                full_messages: favorite_recipe.errors.full_messages.join(', ')
              ),
              :bad_request
            )
          end
        end
      end
    end
  end
end
