module Api
  module V1
    class AuthController < ApplicationController
      def refresh_token_pair
        refresh_token_pair_result = Auth::TokenPairService.new.refresh_and_save(
          user_id: params[:user_id],
          refresh_token: params[:refresh_token],
        )
        return render_error(refresh_token_pair_result.data[:error], :bad_request) if refresh_token_pair_result.failure?

        render(
          json: {
            jwt: refresh_token_pair_result.data[:jwt],
            refresh_token: refresh_token_pair_result.data[:refresh_token],
          }
        )
      end
    end
  end
end
