module Api
  module V1
    class RecipesController < ApplicationController
      def index
        elastic_recipes_query = ElasticQueryBuilders::RecipeQueryBuilder.
          new.
          build(**index_params_hash.except(:page, :items_count))

        elastic_pagination = ElasticPagination.new(
          page: index_params_hash[:page],
          size: index_params_hash[:items_count]
        )
        elastic_recipes_query = elastic_pagination.build_query(query: elastic_recipes_query)

        elastic_recipes_query_result = Recipe.__elasticsearch__.search(elastic_recipes_query)

        @decorated_recipes = elastic_recipes_query_result.
          records.
          includes(:recipe_category, :marks, recipe_ingredients: :ingredient, steps: :tools).
          decorate(context: { current_user: current_user })
        @pagination_options = elastic_pagination.pagination_options(
          query_response: elastic_recipes_query_result.response
        )
      end

      def show
        @decorated_recipe = Recipe.
          includes(:recipe_category, :marks, recipe_ingredients: :ingredient, steps: :tools).
          find(params[:id]).
          decorate(context: { current_user: current_user })
      end

      private

      def index_params_hash
        result = params.
          permit(
            :full_text_value,
            :tools_full_text_value,
            :page,
            :items_count,
            ingredient_ids_include: [],
            ingredient_ids_exclude: [],
            tool_types: [],
          ).
          to_h.
          symbolize_keys

        result[:page] = result[:page].to_i if result[:page].present?
        result[:items_count] = result[:items_count].to_i if result[:items_count].present?

        result
      end
    end
  end
end
