module Api
  class ApplicationController < ActionController::API
    include Pagy::Backend
    include Pundit::Authorization

    class MissingParamsError < StandardError
      attr_reader :params

      def initialize(params)
        @params = params
        super(nil)
      end
    end

    class ExpiredJWTError < StandardError; end
    class InvalidJWTError < StandardError; end

    before_action :validate_api_token!

    rescue_from Pagy::VariableError do
      render_error(I18n.t(:variable_error, scope: 'api_errors.pagy'), :bad_request)
    end

    rescue_from Pagy::OverflowError do
      render_error(I18n.t(:overflow_error, scope: 'api_errors.pagy'), :bad_request)
    end

    rescue_from ActiveRecord::RecordNotFound do
      render_error(I18n.t(:record_not_found, scope: :api_errors), :not_found)
    end

    rescue_from MissingParamsError do |error|
      render_error(I18n.t(:missing_params, params: error.params.join(', '), scope: 'api_errors'), :bad_request)
    end

    rescue_from Pundit::NotAuthorizedError do
      render_error(I18n.t(:action_denied, scope: 'api_errors'), :unauthorized)
    end

    rescue_from ExpiredJWTError do
      render_error(I18n.t(:jwt_expired, scope: :api_errors), :unauthorized)
    end

    rescue_from InvalidJWTError do
      render_error(I18n.t(:jwt_invalid, scope: :api_errors), :unauthorized)
    end

    protected

    def paginate_by_pagy(scope)
      pagy(scope, items: params[:items_count])
    end

    def render_error(message_or_messages, status)
      @errors = message_or_messages.is_a?(Array) ? message_or_messages : [message_or_messages]
      render('api/shared/error', status: status, format: :json)
    end

    def validate_api_token!
      return if request.headers[:Authorization] == Credentials.get(:api_token)

      render_error(I18n.t(:invalid_api_token, scope: :api_errors), :unauthorized)
    end

    def current_user
      @current_user ||= begin
        jwt = request.headers[:JWT]
        return if jwt.nil?

        extract_user_result = Auth::JwtService.new.extract_user(jwt: jwt)
        if extract_user_result.failure?
          if extract_user_result.data[:error] == Auth::JwtService::JWT_EXPIRED_ERROR
            raise ExpiredJWTError
          else
            raise InvalidJWTError
          end
        end

        extract_user_result.data[:user]
      end
    end
  end
end
