class ElasticSyncWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'elasticsearch'

  def perform
    ElasticSync.new.sync_recipes
    ElasticSync.new.sync_ingredients
  end
end
