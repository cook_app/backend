# Tests: spec/elasticsearch/elastic_query_builders/recipe_query_builder_spec.rb

require 'contracts'

module ElasticQueryBuilders
  class RecipeQueryBuilder
    include Contracts::Core
    include Contracts::Builtin

    # Usage example:
    # Recipe.__elasticsearch__.search(ElasticQueryBuilders::RecipeQueryBuilder.new.build(full_text_value: 'помидор', ingredient_ids_exclude: [27, 16], ingredient_ids_exclude: [88, 5]))
    # For pagination use ElasticPagination
    Contract Optional[KeywordArgs[full_text_value: Optional[String], ingredient_ids_include: Optional[ArrayOf[Num]], ingredient_ids_exclude: Optional[ArrayOf[Num]], tool_types: Optional[ArrayOf[String]]]] => Hash
    def build(params = {})
      if params.compact.blank?
        return {
          query: {
            match_all: {},
          },
        }
      end

      query = {
        min_score: 0.5,
        query: {
          bool: {},
        },
      }

      build_full_text_value!(query, params[:full_text_value]) if params[:full_text_value]
      build_ingredient_ids_include!(query, params[:ingredient_ids_include]) if params[:ingredient_ids_include]
      build_ingredient_ids_exclude!(query, params[:ingredient_ids_exclude]) if params[:ingredient_ids_exclude]
      build_tool_types!(query, params[:tool_types]) if params[:tool_types]

      query
    end

    private

    def build_full_text_value!(query, full_text_value)
      query[:query][:bool][:should] ||= []
      query[:query][:bool][:should] << {
        multi_match: {
          query: full_text_value,
          fields: %w[name^2 description^1.5],
        },  
      }
      query[:query][:bool][:should] << {
        nested: {
          path: 'ingredients',
          query: {
            multi_match: {
              query: full_text_value,
              fields: %w[ingredients.name ingredients.description],
            },
          },
        },
      }
    end

    def build_ingredient_ids_include!(query, ingredient_ids_include)
      query[:query][:bool][:filter] ||= []
      query[:query][:bool][:filter] << {
        terms_set: {
          ingredients_ids: {
            terms: ingredient_ids_include,
            minimum_should_match_script: {
              source: 'params.num_terms'
            }
          }
        }
      }
      query[:query][:bool][:should] ||= { match_all: {} }
      query[:sort] ||= ['_score']
      query[:sort].unshift('ingredients_count')
    end

    def build_ingredient_ids_exclude!(query, ingredient_ids_exclude)
      query[:query][:bool][:must_not] = {
        nested: {
          path: 'ingredients',
          query: {
            terms: {
              'ingredients.id': ingredient_ids_exclude,
            },
          },
        },
      }
      query[:query][:bool][:should] ||= { match_all: {} }
    end

    def build_tool_types!(query, tool_types)
      query[:query][:bool][:filter] ||= []
      query[:query][:bool][:filter] << {
        terms_set: {
          tool_types: {
            terms: tool_types,
            minimum_should_match_script: {
              source: 'params.num_terms'
            }
          }
        }
      }
      query[:query][:bool][:should] ||= { match_all: {} }
      query[:sort] ||= ['_score']
      query[:sort].insert(query[:sort].length - 1, 'tool_types_count')
    end
  end
end
