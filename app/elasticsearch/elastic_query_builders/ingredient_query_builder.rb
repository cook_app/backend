require 'contracts'

module ElasticQueryBuilders
  class IngredientQueryBuilder
    include Contracts::Core
    include Contracts::Builtin

    Contract KeywordArgs[full_text_value: String] => Hash
    def build(full_text_value:)
      {
        query: {
          multi_match: {
            query: full_text_value,
            fields: %w[name^2 description],
          },
        },
      }
    end
  end
end
