class ElasticSync
  def sync_recipes
    Recipe.__elasticsearch__.import(
      force: true,
      query: -> { includes(:ingredients) },
    )
  end

  def sync_ingredients
    Ingredient.__elasticsearch__.import(force: true)
  end
end
