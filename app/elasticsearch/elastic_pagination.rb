# Tests: spec/elasticsearch/elastic_pagination_spec.rb

require 'contracts'

class ElasticPagination
  include Contracts::Core
  include Contracts::Builtin

  Contract KeywordArgs[page: Optional[Maybe[Num]], size: Optional[Maybe[Num]]] => Any
  def initialize(page: nil, size: nil)
    @page = page&.positive? ? page : 1
    @size = size.present? ? size.abs : ELASTICSEARCH_DEFAULT_SIZE
  end

  Contract KeywordArgs[query: Optional[Hash]] => Hash
  def build_query(query: {})
    from = @size * @page - @size

    query_with_pagination = query.deep_dup
    query_with_pagination[:from] = from
    query_with_pagination[:size] = @size

    query_with_pagination
  end

  # Return result in pagy's format
  Contract KeywordArgs[query_response: Hash] => Hash
  def pagination_options(query_response:)
    hits_total = query_response.dig('hits', 'total', 'value')
    if @size.zero? || hits_total.nil?
      return {
        total_pages: 0,
        prev_page: nil,
        next_page: nil,
      }
    end

    total_pages = (hits_total / @size.to_f).ceil
    prev_page = [@page - 1, total_pages].min
    {
      total_pages: total_pages,
      prev_page: prev_page < 1 ? nil : prev_page,
      next_page:  @page < total_pages ? @page + 1 : nil,
    }
  end
end
