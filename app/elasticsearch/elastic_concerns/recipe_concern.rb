module ElasticConcerns
  module RecipeConcern
    extend ActiveSupport::Concern

    included do
      include Elasticsearch::Model

      settings(
        number_of_shards: 1,
        analysis: {
          char_filter: {
            ru_char_filter: {
              type: 'mapping',
              mappings: ['Ё=>Е', 'ё=>е'],
            },
          },
          filter: {
            autocomplete_filter: {
              type: 'edge_ngram',
              min_gram: 3,
              max_gram: 20,
            },
          },
          analyzer: {
            russian: {
              type: 'standard',
              stopwords: '_russian_',
            },
            autocomplete: {
              type: 'custom',
              tokenizer: 'lowercase',
              filter: %w[lowercase autocomplete_filter],
              char_filter: %w[ru_char_filter],
            },
          },
        },
      ) do
        mappings dynamic: 'false' do
          indexes :name, type: 'text', analyzer: 'autocomplete'
          indexes :description, type: 'text', analyzer: 'autocomplete'
          indexes :ingredients_count, type: 'integer'
          indexes :ingredients_ids, type: 'integer'
          indexes :tool_types, type: 'text'
          indexes :tool_types_count, type: 'integer'
          indexes :ingredients, type: 'nested' do
            indexes :id, type: 'integer'
            indexes :name, type: 'text', analyzer: 'russian'
            indexes :description, type: 'text', analyzer: 'russian'
          end
        end
      end

      def as_indexed_json(options = {})
        result = as_json(
          only: %i[id name description],
          include: {
            ingredients: {
              only: %i[id name description],
            },
          },
        )
        result['ingredients_count'] = result['ingredients'].count
        result['ingredients_ids'] = result['ingredients'].pluck('id')
        result['tool_types'] = steps.map(&:tools).flatten.pluck(:tool_type).uniq
        result['tool_types_count'] = result['tool_types'].count
        result
      end
    end
  end
end
