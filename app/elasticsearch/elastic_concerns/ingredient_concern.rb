module ElasticConcerns
  module IngredientConcern
    extend ActiveSupport::Concern

    included do
      include Elasticsearch::Model

      settings(
        number_of_shards: 1,
        analysis: {
          char_filter: {
            ru_char_filter: {
              type: 'mapping',
              mappings: ['Ё=>Е', 'ё=>е'],
            },
          },
          filter: {
            autocomplete_filter: {
              type: 'edge_ngram',
              min_gram: 3,
              max_gram: 20,
            },
          },
          analyzer: {
            autocomplete: {
              type: 'custom',
              tokenizer: 'lowercase',
              filter: %w[lowercase autocomplete_filter],
              char_filter: %w[ru_char_filter],
            },
          },
        },
      ) do
        mappings dynamic: 'false' do
          indexes :id, type: 'integer'
          indexes :name, type: 'text', analyzer: 'autocomplete'
          indexes :description, type: 'text', analyzer: 'autocomplete'
        end
      end

      def as_indexed_json(options = {})
        as_json(only: %i[id name description])
      end
    end
  end
end
