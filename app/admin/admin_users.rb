ActiveAdmin.register AdminUser do
  actions :index, :show

  index do
    id_column
    column :email
    column :created_at
  end

  filter :created_at
  filter :email

  show do
    attributes_table do
      row :id
      row :email
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end
end
