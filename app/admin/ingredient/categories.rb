ActiveAdmin.register Ingredient::Category do
  permit_params :name

  index do
    id_column
    column :name
    column :created_at
  end

  filter :name
  filter :created_at

  show do
    attributes_table do
      row :id
      row :name
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    inputs do
      input :name
    end
    actions
  end
end
