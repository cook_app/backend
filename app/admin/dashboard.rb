# frozen_string_literal: true
ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc { I18n.t("active_admin.dashboard") }

  content title: proc { I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel I18n.t(:other, scope: :active_admin) do
          link_to('Sidekiq', sidekiq_web_path, target: :_blank)
        end
      end
    end
  end
end
