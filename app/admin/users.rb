ActiveAdmin.register User do
  config.sort_order = 'created_at_desc'

  actions :index, :show

  index do
    id_column
    column :email
    column :name
    column :created_at
  end

  filter :created_at
  filter :email

  show do
    attributes_table do
      row :id
      row :email
      row :name
      row :avatar do
        image_tag(user.avatar_url(:small))
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  action_item :oauth_identifiers, only: :show do
    link_to(I18n.t('activerecord.models.oauth/identifier.other'), admin_oauth_identifiers_path(q: { user_id_eq: user.id }))
  end

  action_item :marks, only: :show do
    link_to(I18n.t('activerecord.models.mark.other'), admin_marks_path(q: { user_id_eq: user.id }))
  end
end
