ActiveAdmin.register Mark do
  actions :index, :show

  index do
    id_column
    column :user
    column :recipe
    column :value
    column :created_at
  end

  filter :value
  filter :user, collection: -> { User.joins(:marks).distinct }
  filter :recipe, collection: -> { Recipe.joins(:marks).distinct }
  filter :created_at

  show do
    attributes_table do
      row :id
      row :user
      row :recipe
      row :value
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end
end
