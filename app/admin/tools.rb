ActiveAdmin.register Tool do
  decorate_with ToolDecorator

  permit_params :tool_type, :brand, :tool_model

  index do
    id_column
    column :tool_type_translated
    column :brand
    column :tool_model
    column :created_at
  end

  filter :tool_type,
    as: :select,
    collection: -> {
      Tool.all.pluck(:tool_type).uniq.map { |tool_type| [I18n.t(tool_type, scope: 'activerecord.attributes.tool.tool_types'), tool_type] }
    }
  filter :brand, as: :select, collection: -> { Tool.all.pluck(:brand).uniq.compact }
  filter :tool_model
  filter :created_at

  show title: :id do
    attributes_table do
      row :id
      row :tool_type_translated
      row :brand
      row :tool_model
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    inputs do
      input :tool_type,
        as: :select,
        collection: Tool.all.pluck(:tool_type).uniq.map { |tool_type| [I18n.t(tool_type, scope: 'activerecord.attributes.tool.tool_types'), tool_type] }
      input :brand
      input :tool_model
    end
    actions
  end
end
