ActiveAdmin.register Oauth::Identifier do
  actions :index, :show

  index do
    id_column
    column :user
    column :provider
    column :created_at
  end

  filter :user, collection: -> { User.all.joins(:oauth_identifiers) }
  filter :provider, as: :select, collection: -> { Oauth::Identifier.providers.values }
  filter :created_at

  show do
    attributes_table do
      row :id
      row :user
      row :provider
      row :provider_user_id
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end
end
