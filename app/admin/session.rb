# frozen_string_literal: true

ActiveAdmin.register_page 'Session' do
  menu false

  controller do
    private

    # Hide menu
    def current_menu
      Struct.new(:items).new([])
    end
  end

  content title: proc { I18n.t('active_admin.session') } do
    active_admin_form_for '', url: '/admin/session/create' do |form|
      form.inputs do
        form.input :email
        form.input :password
      end
      form.actions do
        form.action :submit, as: :button
      end
    end
  end

  page_action :create, method: :post do
    permitted_params = params.permit(:email, :password)

    admin_user = AdminUser.find_by(email: permitted_params[:email])
    is_password_correct = AdminUsers::PasswordService.
      new.
      correct?(encrypted_password: admin_user&.encrypted_password, password: permitted_params[:password])

    if admin_user.nil? || !is_password_correct
      flash[:error] = I18n.t('active_admin.sessions.invalid_email_or_password')
      return render(:index)
    end

    session[:admin_user_id] = admin_user.id
    redirect_to(admin_root_path)
  end

  page_action :destroy, method: :delete do
    session[:admin_user_id] = nil
    redirect_to(admin_session_path)
  end
end
