class Auth::RefreshToken < ApplicationRecord
  self.table_name = 'auth_refresh_tokens'

  belongs_to :user

  validates :encrypted_refresh_token, presence: true
  validates :expires_at, presence: true
end
