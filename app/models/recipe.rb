class Recipe < ApplicationRecord
  include RecipeImageUploader::Attachment(:image)
  include ElasticConcerns::RecipeConcern

  has_many :recipe_ingredients, class_name: 'Recipe::Ingredient'
  has_many :ingredients, through: :recipe_ingredients
  has_many :steps
  has_many :marks

  belongs_to :recipe_category, class_name: 'Recipe::Category'

  validates :name, presence: true
  validates :kilocalories, presence: true
  validates :fats, presence: true
  validates :carbohydrates, presence: true
  validates :proteins, presence: true
end
