class Ingredient < ApplicationRecord
  include IngredientImageUploader::Attachment(:image)
  include ElasticConcerns::IngredientConcern

  has_many :recipes_ingredients

  belongs_to :ingredient_category, class_name: 'Ingredient::Category'

  validates :name, presence: true

  enum unit: {
    grams: 'grams',
    milliliters: 'milliliters',
    amount: 'amount',
  }
end
