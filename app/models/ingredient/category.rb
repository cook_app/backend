class Ingredient::Category < ApplicationRecord
  include IngredientCategoryImageUploader::Attachment(:image)

  has_many :ingredients, class_name: 'Ingredient'
end
