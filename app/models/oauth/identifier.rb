class Oauth::Identifier < ApplicationRecord
  self.table_name = 'oauth_identifiers'

  belongs_to :user

  validates :provider, presence: true, uniqueness: { scope: :user_id }
  validates :provider_user_id, presence: true, uniqueness: { scope: :provider }

  enum provider: {
    developer: 'developer',
    google: 'google',
  }
end
