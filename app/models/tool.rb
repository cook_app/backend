class Tool < ApplicationRecord
  has_and_belongs_to_many :steps, join_table: 'step_tools'

  enum tool_type: {
    knife: 'knife', # Нож
    frying_pan: 'frying_pan', # Сковорода
    pot: 'pot', # Кастрюля
    mixer: 'mixer', # Миксер
    oven: 'oven', # Духовка
    electric_grill: 'electric_grill', # Электрогриль
    multicooker: 'multicooker', # Мультиварка
    cutting_board: 'cutting_board', # Разделочная доска
  }

  validates :tool_type, presence: true, uniqueness: { scope: %i[brand tool_model] }
end
