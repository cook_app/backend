class Recipe::Category < ApplicationRecord
  include RecipeCategoryImageUploader::Attachment(:image)

  has_many :recipes, class_name: 'Recipe'
end
