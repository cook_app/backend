class Recipe::Ingredient < ApplicationRecord
  belongs_to :recipe, class_name: 'Recipe'
  belongs_to :ingredient, class_name: '::Ingredient'

  validates :amount, presence: true
  validates :recipe_id, uniqueness: { scope: :ingredient_id }
end
