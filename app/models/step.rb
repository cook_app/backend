class Step < ApplicationRecord
  include BaseImageUploader::Attachment(:image)

  belongs_to :recipe

  has_and_belongs_to_many :tools, join_table: 'step_tools'

  validates :content, presence: true
  validates :position, numericality: { greater_than_or_equal_to: 1 }
end
