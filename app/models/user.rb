class User < ApplicationRecord
  include UserAvatarUploader::Attachment(:avatar)

  self.implicit_order_column = 'created_at'

  has_one :auth_refresh_token, class_name: 'Auth::RefreshToken'

  has_many :oauth_identifiers, class_name: 'Oauth::Identifier'
  has_many :favorite_recipes
  has_many :marks

  validates :email, presence: true, uniqueness: true
  validates :name, presence: true
end
