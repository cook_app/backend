# frozen_string_literal: true

# Integration tests: spec/services/auth/token_pair_service_spec.rb

module Auth
  class TokenPairService
    REFRESH_TOKEN_EXPIRES_AFTER_DAYS = 10

    def initialize(jwt_service: nil, refresh_token_service: nil)
      @jwt_service = jwt_service || JwtService.new
      @refresh_token_service = refresh_token_service || RefreshTokenService.new
    end

    def generate_and_save(
      user:,
      jwt_expires_after_hours: JwtService::JWT_EXPIRES_AFTER_HOURS,
      refresh_token_expires_after_days: REFRESH_TOKEN_EXPIRES_AFTER_DAYS,
      current_time: Time.current
    )
      jwt_encoding_result = @jwt_service.encode_user(user: user, jwt_expires_after_hours: jwt_expires_after_hours)
      return jwt_encoding_result if jwt_encoding_result.failure?

      refresh_token_generation_result = @refresh_token_service.generate
      return refresh_token_generation_result if refresh_token_generation_result.failure?

      jwt = jwt_encoding_result.data[:jwt]
      refresh_token = refresh_token_generation_result.data[:refresh_token]
      encrypted_refresh_token = refresh_token_generation_result.data[:encrypted_refresh_token]

      begin
        ApplicationRecord.transaction do
          user.auth_refresh_token.destroy! if user.auth_refresh_token.present?
          Auth::RefreshToken.create!(
            user: user,
            encrypted_refresh_token: encrypted_refresh_token,
            expires_at: current_time + refresh_token_expires_after_days.days,
          )
        end
      rescue ActiveRecord::RecordInvalid
        # TODO: send to sentry
        return Result.new(false, { error: "Can't create Auth::RefreshToken, check sentry" })
      end

      Result.new(true, { jwt: jwt, refresh_token: refresh_token })
    end

    def refresh_and_save(
      user_id:,
      refresh_token:,
      jwt_expires_after_hours: JwtService::JWT_EXPIRES_AFTER_HOURS,
      refresh_token_expires_after_days: REFRESH_TOKEN_EXPIRES_AFTER_DAYS,
      current_time: Time.current
    )
      return Result.new(false, { error: 'Params are blank' }) if user_id.nil? || refresh_token.nil?

      user = User.find_by(id: user_id)
      auth_refresh_token = user&.auth_refresh_token

      if auth_refresh_token.nil? || BCrypt::Password.new(auth_refresh_token.encrypted_refresh_token) != refresh_token
        return Result.new(false, { error: 'Refresh token is invalid' })
      elsif auth_refresh_token.expires_at < current_time
        return Result.new(false, { error: 'Refresh token expired' })
      end

      generate_and_save(
        user: user,
        jwt_expires_after_hours: jwt_expires_after_hours,
        refresh_token_expires_after_days: refresh_token_expires_after_days,
        current_time: current_time,
      )
    end
  end
end
