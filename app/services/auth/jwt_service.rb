# frozen_string_literal: true

# Function tests: spec/services/auth/jwt_service_spec.rb
# Function tests: spec/services/auth/token_pair_service_spec.rb

module Auth
  class JwtService
    JWT_EXPIRES_AFTER_HOURS = 8
    JWT_EXPIRED_ERROR = 'JWT expired'
    
    def initialize(secret_key_base: Rails.application.secret_key_base, algorithm: 'HS256')
      @secret_key_base = secret_key_base
      @algorithm = algorithm
    end

    def encode_user(user:, jwt_expires_after_hours: JWT_EXPIRES_AFTER_HOURS, current_time: Time.current)
      return Result.new(false, { error: 'User is nil' }) if user.nil?

      payload = {
        user: {
          id: user.id,
          email: user.email,
          name: user.name,
          avatar_url: user.avatar_url(:medium),
        },
        exp: current_time.to_i + jwt_expires_after_hours * 3600,
      }

      jwt = JWT.encode(payload, @secret_key_base, @algorithm)

      Result.new(true, { jwt: jwt})
    end

    def extract_user(jwt:)
      return Result.new(false, { error: 'JWT is blank' }) if jwt.blank?

      decoded_jwt = JWT.decode(
        jwt,
        @secret_key_base,
        true,
        {
          algorithm: @algorithm,
        }
      )

      user = User.find_by(id: decoded_jwt.first.dig('user', 'id'))
      return Result.new(false, { error: 'User not found' }) if user.nil?

      Result.new(true, { user: user })
    rescue JWT::ExpiredSignature
      Result.new(false, { error: JWT_EXPIRED_ERROR })
    rescue JWT::VerificationError
      Result.new(false, { error: 'JWT verification failed' })
    rescue JWT::DecodeError
      Result.new(false, { error: 'JWT invalid format' })
    end
  end
end
