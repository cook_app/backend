# frozen_string_literal: true

# Function tests: spec/services/auth/token_pair_service_spec.rb

module Auth
  class RefreshTokenService
    def generate
      refresh_token = SecureRandom.hex(32)

      Result.new(
        true,
        {
          refresh_token: refresh_token,
          encrypted_refresh_token: BCrypt::Password.create(refresh_token),
        }
      )
    end
  end
end
