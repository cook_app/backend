# Tests: spec/services/oauth/developer_auth_service_spec.rb
# Tests: spec/services/oauth/google_auth_service_spec.rb

require 'contracts'

module Oauth
  class BaseAuthService
    include Contracts::Core
    include Contracts::Builtin

    Contract KeywordArgs[params: Or[Hash, ActionController::Parameters]] => Result
    def call(params:)
      raise NotImplementedError
    end

    protected

    def find_or_create_user(provider:, provider_user_id:, email:, name:, avatar_remote_url: nil)
      oauth_identifier = Oauth::Identifier.find_or_initialize_by(provider: provider, provider_user_id: provider_user_id)
      return Result.new(true, { user: oauth_identifier.user }) if oauth_identifier.persisted?

      user = User.find_by(email: email)

      begin
        ApplicationRecord.transaction do
          user ||= User.create!(
            email: email,
            name: name,
            avatar_remote_url: avatar_remote_url,
          )
          oauth_identifier.user = user
          oauth_identifier.save!
        end
      rescue => exception
        # TODO: send to sentry
        error = if Rails.env.development?
          "Can not create user with such provider, message: #{exception.message}"
        else
          'Can not create user with such provider'
        end
        return Result.new(false, { error: error })
      end

      Result.new(true, { user: user })
    end
  end
end
