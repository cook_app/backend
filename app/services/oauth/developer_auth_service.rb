# Tests: spec/services/oauth/developer_auth_service_spec.rb

module Oauth
  class DeveloperAuthService < BaseAuthService
    Contract KeywordArgs[params: Or[Hash, ActionController::Parameters]] => Result
    def call(params:)
      provider_user_id = params['provider_user_id']
      email = params['email']
      name = params['name']
      return Result.new(false, { error: 'Data not found' }) if provider_user_id.blank? || email.blank? || name.blank?

      find_or_create_user(
        provider: ::Oauth::Identifier.providers['developer'],
        provider_user_id: provider_user_id,
        email: email,
        name: name,
      )
    end
  end
end
