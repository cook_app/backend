# Tests: spec/services/oauth/google_auth_service_spec.rb

module Oauth
  class GoogleAuthService < BaseAuthService
    def initialize(validator: nil, oauth_google_ios_client_id: nil)
      @validator = validator || GoogleIDToken::Validator.new
      @oauth_google_ios_client_id = oauth_google_ios_client_id || Credentials.get(:oauth_google_ios_client_id)
    end

    Contract KeywordArgs[params: Or[Hash, ActionController::Parameters]] => Result
    def call(params:)
      token = params['token']
      return Result.new(false, { error: 'Token not found' }) if token.blank?

      payload = nil
      [@oauth_google_ios_client_id].each do |client_id|
        payload = validate_and_get_payload(token, client_id)
        break if payload.present?
      end
      return Result.new(false, { error: 'Token invalid' }) if payload.nil?

      find_or_create_user(
        provider: ::Oauth::Identifier.providers['google'],
        provider_user_id: payload['sub'],
        email: payload['email'],
        name: payload['name'],
        avatar_remote_url: payload['picture'],
      )
    end

    private

    def validate_and_get_payload(token, client_id)
      @validator.check(token, client_id)
    rescue => exception
      return nil if exception.class.name.starts_with?('GoogleIDToken::')

      raise exception
    end
  end
end
