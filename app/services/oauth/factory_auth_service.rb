# frozen_string_literal: true

# Tests: spec/services/oauth/factory_auth_service_spec.rb

module Oauth
  class FactoryAuthService
    AUTH_SERVICE_CLASSES = {
      ::Oauth::Identifier.providers['google'] => GoogleAuthService,
      ::Oauth::Identifier.providers['developer'] => DeveloperAuthService,
    }

    def initialize(env: nil)
      @env = env || Rails.env
    end

    def call(provider:)
      if @env != 'development' && provider == ::Oauth::Identifier.providers['developer']
        return Result.new(false, { error: 'Provider available in development only' })
      end

      auth_service_class = AUTH_SERVICE_CLASSES[provider]
      return Result.new(false, { error: 'Wrong provider' }) if auth_service_class.nil?

      Result.new(true, { auth_service: auth_service_class.new })
    end
  end
end
