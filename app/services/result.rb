class Result
  def initialize(is_success, data = nil)
    @is_success = is_success
    @data = data
  end

  attr_reader :data

  def success?
    @is_success
  end

  def failure?
    !success?
  end
end
