module Recipes
  class RatingService
    def calculate(marks:)
      return if marks.blank?

      (marks.pluck(:value).sum / marks.count.to_f).round(1)
    end
  end
end
