class Credentials
  class << self
    def get(key)
      if Rails.env.production?
        Rails.application.credentials.dig(:production, key.downcase.to_sym)
      else
        ENV[key.upcase.to_s]
      end
    end
  end
end
