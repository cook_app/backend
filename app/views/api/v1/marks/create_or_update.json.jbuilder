json.mark do
  json.partial! 'mark', mark: @mark
end
json.recipe do
  json.rating @recipe_rating
end
