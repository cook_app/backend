json.partial! 'recipe', decorated_recipe: @decorated_recipe
json.recipe_ingredients do
  json.array! @decorated_recipe.recipe_ingredients do |recipe_ingredient|
    json.partial! 'api/v1/recipe_ingredients/recipe_ingredient', recipe_ingredient: recipe_ingredient
    json.ingredient do
      json.partial! 'api/v1/ingredients/ingredient', ingredient: recipe_ingredient.ingredient
    end
  end
end
json.steps do
  json.array! @decorated_recipe.steps do |step|
    json.partial! 'api/v1/steps/step', step: step
    json.tools do
      json.array! step.tools do |tool|
        json.partial! 'api/v1/tools/tool', decorated_tool: tool.decorate
      end
    end
  end
end
json.category do
  json.partial! 'api/v1/recipe_categories/recipe_category', recipe_category: @decorated_recipe.recipe_category
end
if @decorated_recipe.my_mark.present?
  json.my_mark do
    json.partial! 'api/v1/marks/mark', mark: @decorated_recipe.my_mark
  end
end
