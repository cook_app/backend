json.(
  decorated_recipe,
  :id,
  :name,
  :description,
  :kilocalories,
  :fats,
  :carbohydrates,
  :proteins,
  :duration_seconds,
  :is_favorite,
  :tool_types_translated_uniq,
  :rating,
)

json.image_url decorated_recipe.image_url(:medium)
