json.recipes do
  json.array! @decorated_recipes do |decorated_recipe|
    json.partial! 'recipe', decorated_recipe: decorated_recipe
    json.category do
      json.partial! 'api/v1/recipe_categories/recipe_category', recipe_category: decorated_recipe.recipe_category
    end
    json.recipe_ingredients do
      json.array! decorated_recipe.recipe_ingredients do |recipe_ingredient|
        json.partial! 'api/v1/recipe_ingredients/recipe_ingredient', recipe_ingredient: recipe_ingredient
        json.ingredient do
          json.partial! 'api/v1/ingredients/ingredient', ingredient: recipe_ingredient.ingredient
        end
      end
    end
    if decorated_recipe.my_mark.present?
      json.my_mark do
        json.partial! 'api/v1/marks/mark', mark: decorated_recipe.my_mark
      end
    end
  end
end
json.partial! 'api/v1/pagination/elastic', pagination_options: @pagination_options
