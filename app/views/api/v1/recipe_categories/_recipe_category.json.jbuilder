json.(
  recipe_category,
  :id,
  :name,
)

json.image_url recipe_category.image_url(:medium)
