json.pagination do
  json.total_pages pagination_options[:total_pages]
  json.prev_page pagination_options[:prev_page]
  json.next_page pagination_options[:next_page]
end
