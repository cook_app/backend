json.tools do
  json.array! @decorated_tools do |decorated_tool|
    json.partial! 'tool', decorated_tool: decorated_tool
  end
end
json.partial! 'api/v1/pagination/pagy', pagy: @pagy
