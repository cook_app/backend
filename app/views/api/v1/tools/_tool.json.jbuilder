json.(
  decorated_tool,
  :id,
  :tool_type,
  :tool_type_translated,
  :brand,
  :tool_model,
)
