json.(
  step,
  :id,
  :name,
  :content,
  :duration_seconds,
  :position,
)

json.image_url step.image_url(:medium)
