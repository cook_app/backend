json.(
  ingredient,
  :id,
  :name,
  :description,
  :unit,
)

json.image_url ingredient.image_url(:medium)
