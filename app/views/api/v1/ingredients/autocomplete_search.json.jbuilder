json.ingredients do
  json.array! @elastic_ingredients do |elastic_ingredient|
    json.(
      elastic_ingredient,
      :id,
      :name,
      :description,
    )
  end
end
json.partial! 'api/v1/pagination/elastic', pagination_options: @pagination_options
