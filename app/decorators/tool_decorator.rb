class ToolDecorator < Draper::Decorator
  delegate_all

  def tool_type_translated
    I18n.t(tool_type, scope: 'activerecord.attributes.tool.tool_types')
  end
end
