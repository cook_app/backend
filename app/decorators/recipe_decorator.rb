class RecipeDecorator < Draper::Decorator
  delegate_all

  def duration_seconds
    steps.reduce(0) { |duration_seconds_sum, step| duration_seconds_sum + step.duration_seconds }
  end

  def is_favorite
    return false if context[:current_user].nil?

    context[:current_user].favorite_recipes.pluck(:recipe_id).include?(id)
  end

  def tool_types_translated_uniq
    steps.map(&:tools).flatten.map do |tool|
      I18n.t(tool.tool_type, scope: 'activerecord.attributes.tool.tool_types')
    end.uniq
  end

  def rating
    Recipes::RatingService.new.calculate(marks: marks)
  end

  def my_mark
    return if context[:current_user].nil?

    context[:current_user].marks.find_by(recipe: self)
  end
end
