module Tools
  class IndexQuery
    def call(only_unique_tool_types: nil)
      Tool.where(only_unique_tool_types ? { brand: nil, tool_model: nil } : nil)
    end
  end
end
