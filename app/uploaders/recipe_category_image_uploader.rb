class RecipeCategoryImageUploader < BaseImageUploader
  Attacher.default_url do |**options|
    "/images/default_recipe_category_#{options[:derivative] || 'medium'}.jpg"
  end
end
