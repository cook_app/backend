class IngredientImageUploader < BaseImageUploader
  Attacher.default_url do |**options|
    "/images/default_ingredient_#{options[:derivative] || 'medium'}.jpg"
  end
end
