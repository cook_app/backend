class UserAvatarUploader < BaseImageUploader
  Attacher.default_url do |**options|
    "/images/default_user_#{options[:derivative] || 'medium'}.jpg"
  end
end
