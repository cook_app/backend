class IngredientCategoryImageUploader < BaseImageUploader
  Attacher.default_url do |**options|
    "/images/default_ingredient_category_#{options[:derivative] || 'medium'}.jpg"
  end
end
