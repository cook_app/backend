class RecipeImageUploader < BaseImageUploader
  Attacher.default_url do |**options|
    "/images/default_recipe_#{options[:derivative] || 'medium'}.jpg"
  end
end
