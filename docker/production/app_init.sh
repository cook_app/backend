#!/bin/sh

echo 'Creating db...'
rake db:create
echo 'Execute migrations...'
rake db:migrate
echo 'Seed db...'
rake db:seed


echo 'Remove public_docker_volume...'
rm -rf public_docker_volume/*
echo 'Copy public to public_docker_volume...'
cp -R public/* public_docker_volume/

echo 'Done'
