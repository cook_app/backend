# About

Backend для мобильного приложения с рецептами.

Stack: rails 6, docker, postgresql, redis, elasticsearch, sidekiq, activeadmin, shrine, gitlab CI/CD, rspec.

Админка готова только частично, как и само приложение хех.

Для аутентификации используется google oauth, в дальнейшем будут добавлены другие.

Авторизация происходит с помощью JWT.

# How to start

1. install docker-compose
2. `docker-compose -f docker-compose.development.yml up`
3. open new tab in the terminal
4. install rvm
5. `rvm install 3.0.2`
6. go to the project directory
7. `rvm use`
9. `bundle`
8. `rake db:create && rake db:migrate && rake db:seed`
10. `rails s` 

# Api документация

Insomnia.json

# Особенности моделей

## Steps

`position` начинается с 1

## Ingredients

Возможные значения `unit`:

- grams - граммы
- piece - штуки
- milliliters - милилитры
