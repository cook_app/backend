require 'rails_helper'

RSpec.describe Auth::TokenPairService, type: :service do
  describe '#generate_and_save' do
    subject {
      described_class.
        new.
        generate_and_save(
          user: subject_user,
          jwt_expires_after_hours: subject_jwt_expires_after_hours,
          refresh_token_expires_after_days: subject_refresh_token_expires_after_days,
          current_time: subject_current_time,
        )
    }

    shared_examples 'success method execution' do
      it '- generates, saves and returns new token pair' do
        token_pair_result = subject

        aggregate_failures('check data returned from the method') do
          expect(token_pair_result.success?).to(be(true))
          expect(token_pair_result.data[:jwt].present?).to(be(true))
          expect(token_pair_result.data[:refresh_token].present?).to(be(true))
        end

        subject_user.reload
        aggregate_failures('check the database') do
          expect(subject_user.auth_refresh_token&.encrypted_refresh_token&.present?).to(be(true))
          expect(subject_user.auth_refresh_token&.expires_at).to(eq(subject_current_time + subject_refresh_token_expires_after_days.days))
          expect(Auth::RefreshToken.all.count).to(eq(expected_auth_refresh_tokens_count))
        end
      end
    end

    shared_examples 'failure method execution' do
      it "- returns failure result object and doesn't change database" do
        token_pair_result = subject

        aggregate_failures('check data returned from method') do
          expect(token_pair_result.failure?).to(be(true))
          expect(token_pair_result.data[:error].present?).to(be(true))
          expect(token_pair_result.data.keys.count).to(eq(1))
        end

        aggregate_failures('check the database') do
          expect(Auth::RefreshToken.all.count).to(eq(expected_auth_refresh_tokens_count))
        end
      end
    end

    let!(:subject_jwt_expires_after_hours) { 2 }
    let!(:subject_refresh_token_expires_after_days) { 1 }
    let!(:subject_current_time) { Time.parse('20.01.2022 10:00') }

    context 'has two users' do
      let!(:users) {
        [
          create(:user),
          create(:user),
        ]
      }

      context 'and has no refresh tokens' do
        context 'and pass to the method valid user' do
          let!(:subject_user) { users.last }
          let!(:expected_auth_refresh_tokens_count) { 1 }

          include_examples 'success method execution'
        end

        context 'and pass to the method nil user' do
          let!(:subject_user) { nil }
          let!(:expected_auth_refresh_tokens_count) { 0 }

          include_examples 'failure method execution'
        end
      end

      context 'and users have auth refresh tokens' do
        let!(:auth_refresh_tokens) {
          [
            create(:auth_refresh_token, user: users.first, expires_at: subject_current_time + 1.hour),
            create(:auth_refresh_token, user: users.second, expires_at: subject_current_time + 1.hour),
          ]
        }

        context 'and pass to the method valid user' do
          let!(:subject_user) { users.last }
          let!(:expected_auth_refresh_tokens_count) { 2 }

          include_examples 'success method execution'
        end

        context 'and pass to the method nil user' do
          let!(:subject_user) { nil }
          let!(:expected_auth_refresh_tokens_count) { 2 }

          include_examples 'failure method execution'
        end
      end
    end
  end

  describe '#refresh_and_save' do
    subject {
      described_class.
        new.
        refresh_and_save(
          user_id: subject_user_id,
          refresh_token: subject_refresh_token,
          jwt_expires_after_hours: subject_jwt_expires_after_hours,
          refresh_token_expires_after_days: subject_refresh_token_expires_after_days,
          current_time: subject_current_time,
        )
    }

    shared_examples 'success method execution' do
      it '- generates, saves and returns new token pair' do
        refresh_token_pair_result = subject

        aggregate_failures('check data returned from the method') do
          expect(refresh_token_pair_result.success?).to(be(true))
          expect(refresh_token_pair_result.data[:jwt].present?).to(be(true))
          expect(refresh_token_pair_result.data[:refresh_token].present?).to(be(true))
        end

        subject_user.reload
        aggregate_failures('check the database') do
          expect(subject_user.auth_refresh_token&.encrypted_refresh_token&.present?).to(be(true))
          expect(subject_user.auth_refresh_token&.encrypted_refresh_token).to_not(eq(old_encrypted_refresh_token))
          expect(subject_user.auth_refresh_token&.expires_at).to(eq(subject_current_time + subject_refresh_token_expires_after_days.days))
          expect(Auth::RefreshToken.all.count).to(eq(expected_auth_refresh_tokens_count))
        end
      end
    end

    shared_examples 'failure method execution' do
      it "- returns failure result object and doesn't change database" do
        token_pair_result = subject

        aggregate_failures('check data returned from method') do
          expect(token_pair_result.failure?).to(be(true))
          expect(token_pair_result.data[:error].present?).to(be(true))
          expect(token_pair_result.data.keys.count).to(eq(1))
        end

        subject_user&.reload
        aggregate_failures('check the database') do
          expect(subject_user&.auth_refresh_token&.encrypted_refresh_token).to(eq(expected_subject_user_encrypted_refresh_token))
          expect(subject_user&.auth_refresh_token&.expires_at).to(eq(expected_subject_user_expires_at))
          expect(Auth::RefreshToken.all.count).to(eq(expected_auth_refresh_tokens_count))
        end
      end
    end

    let!(:subject_jwt_expires_after_hours) { 2 }
    let!(:subject_refresh_token_expires_after_days) { 1 }
    let!(:subject_current_time) { Time.parse('20.01.2022 10:00') }

    context 'has two users' do
      let!(:users) {
        [
          create(:user),
          create(:user),
        ]
      }

      context 'and has no refresh tokens' do
        context 'and pass to the method valid user_id and invalid refresh_token' do
          let!(:subject_user) { users.last }
          let!(:subject_user_id) { subject_user.id }
          let!(:subject_refresh_token) { 'invalid_refresh_token' }
          let!(:expected_auth_refresh_tokens_count) { 0 }
          let!(:expected_subject_user_encrypted_refresh_token) { nil }
          let!(:expected_subject_user_expires_at) { nil }

          include_examples 'failure method execution'
        end

        context 'and pass to the method invalid user_id and invalid refresh_token' do
          let!(:subject_user) { nil }
          let!(:subject_user_id) { 'invalid_user_id' }
          let!(:subject_refresh_token) { 'invalid_refresh_token' }
          let!(:expected_auth_refresh_tokens_count) { 0 }
          let!(:expected_subject_user_encrypted_refresh_token) { nil }
          let!(:expected_subject_user_expires_at) { nil }

          include_examples 'failure method execution'
        end
      end

      context 'and users have auth refresh tokens' do
        let!(:refresh_tokens_data) {
          first_refresh_token = SecureRandom.hex(32)
          second_refresh_token = SecureRandom.hex(32)
          [
            {
              refresh_token: first_refresh_token,
              auth_refresh_token: create(:auth_refresh_token, refresh_token: first_refresh_token, user: users.first, expires_at: subject_current_time + 1.hour),
            },
            {
              refresh_token: second_refresh_token,
              auth_refresh_token: create(:auth_refresh_token, refresh_token: second_refresh_token, user: users.second, expires_at: subject_current_time + 1.hour),
            },
          ]
        }

        context 'and pass to the method valid user_id and valid refresh_token' do
          let!(:subject_user) { users.second }
          let!(:subject_user_id) { subject_user.id }
          let!(:subject_refresh_token) { refresh_tokens_data.second[:refresh_token] }
          let!(:old_encrypted_refresh_token) { refresh_tokens_data.second[:auth_refresh_token].encrypted_refresh_token }
          let!(:expected_auth_refresh_tokens_count) { 2 }

          include_examples 'success method execution'
        end

        context 'and pass to the method valid user_id and invalid refresh_token' do
          let!(:subject_user) { users.second }
          let!(:subject_user_id) { subject_user.id }
          let!(:subject_refresh_token) { 'invalid_refresh_token' }
          let!(:expected_auth_refresh_tokens_count) { 2 }
          let!(:expected_subject_user_encrypted_refresh_token) { refresh_tokens_data.second[:auth_refresh_token].encrypted_refresh_token }
          let!(:expected_subject_user_expires_at) { refresh_tokens_data.second[:auth_refresh_token].expires_at }

          include_examples 'failure method execution'
        end

        context 'and pass to the method invalid user_id and valid refresh_token' do
          let!(:subject_user) { nil }
          let!(:subject_user_id) { 'invalid_user_id' }
          let!(:subject_refresh_token) { refresh_tokens_data.second[:refresh_token] }
          let!(:expected_auth_refresh_tokens_count) { 2 }
          let!(:expected_subject_user_encrypted_refresh_token) { nil }
          let!(:expected_subject_user_expires_at) { nil }

          include_examples 'failure method execution'
        end
      end

      context 'and second user has expired auth refresh token' do
        let!(:second_user_refresh_token) { SecureRandom.hex(32) }
        let!(:second_user_auth_refresh_token) {
          create(:auth_refresh_token, refresh_token: second_user_refresh_token, user: users.second, expires_at: subject_current_time - 1.minute)
        }

        context 'and pass to the method valid user_id and valid refresh_token' do
          let!(:subject_user) { users.second }
          let!(:subject_user_id) { subject_user.id }
          let!(:subject_refresh_token) { second_user_refresh_token }
          let!(:expected_auth_refresh_tokens_count) { 1 }
          let!(:expected_subject_user_encrypted_refresh_token) { second_user_auth_refresh_token.encrypted_refresh_token }
          let!(:expected_subject_user_expires_at) { second_user_auth_refresh_token.expires_at }

          include_examples 'failure method execution'
        end
      end
    end
  end
end
