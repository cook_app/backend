require 'rails_helper'

RSpec.describe Auth::JwtService, type: :service do
  describe '#encode_user' do
    subject {
      described_class.
        new(
          secret_key_base: 'secret_key_base',
          algorithm: 'HS256',
        ).
        encode_user(
          user: subject_user,
          jwt_expires_after_hours: subject_jwt_expires_after_hours,
          current_time: subject_current_time,
        )
    }

    shared_examples 'success method execution' do
      it '- returns JWT with correct payload' do
        jwt_encoding_result = subject

        aggregate_failures('check data returned from the method') do
          expect(jwt_encoding_result.success?).to(be(true))
          expect(jwt_encoding_result.data[:jwt].present?).to(be(true))

          payload_base_64 = jwt_encoding_result.data[:jwt].split('.').second
          expect(payload_base_64.present?).to(be(true))

          payload = JSON.parse(Base64.decode64(payload_base_64))
          expect(payload).to(eq(expected_payload))
        end
      end
    end

    shared_examples 'failure method execution' do
      it '- returns failure result object' do
        jwt_encoding_result = subject

        aggregate_failures('check data returned from the method') do
          expect(jwt_encoding_result.failure?).to(be(true))
          expect(jwt_encoding_result.data[:error].present?).to(be(true))
          expect(jwt_encoding_result.data.keys.count).to(eq(1))
        end
      end
    end

    let!(:subject_jwt_expires_after_hours) { 2 }
    let!(:subject_current_time) { Time.parse('20.01.2022 10:00') }

    context 'has one user' do
      let!(:user) { create(:user, name: 'User 1') }

      context 'pass the user' do
        let!(:subject_user) { user }
        let!(:expected_payload) {
          {
            'user' => {
              'id' => subject_user.id,
              'email' => subject_user.email,
              'name' => subject_user.name,
              'avatar_url' => subject_user.avatar_url(:medium),
            },
            'exp' => subject_current_time.to_i + subject_jwt_expires_after_hours * 3600,
          }
        }

        include_examples 'success method execution'
      end

      context 'pass nil as a user' do
        let!(:subject_user) { nil }

        include_examples 'failure method execution'
      end
    end
  end

  describe '#extract_user' do
    subject {
      described_class.
        new(
          secret_key_base: subject_secret_key_base,
          algorithm: subject_algorithm,
        ).
        extract_user(jwt: subject_jwt)
    }

    shared_examples 'success method execution' do
      it '- returns correct user' do
        extracting_user_result = subject

        aggregate_failures('check data returned from the method') do
          expect(extracting_user_result.success?).to(be(true))
          expect(extracting_user_result.data[:user]&.id).to(eq(expected_user.id))
        end
      end
    end

    shared_examples 'failure method execution' do
      it '- returns an error' do
        extracting_user_result = subject

        aggregate_failures('check data returned from the method') do
          expect(extracting_user_result.failure?).to(be(true))
          expect(extracting_user_result.data[:error].present?).to(be(true))
          expect(extracting_user_result.data.keys.count).to(eq(1))
        end
      end
    end

    let!(:subject_secret_key_base) { 'secret_key_base' }
    let!(:subject_algorithm) { 'HS256' }

    context 'db contains two users' do
      let!(:users) {
        [
          create(:user, email: 'user1@example.com', name: 'User 1'),
          create(:user, email: 'user2@example.com', name: 'User 2'),
        ]
      }

      context 'pass valid jwt' do
        let!(:expected_user) { users.second }
        let!(:subject_jwt) {
          payload = {
            'user' => {
              'id' => expected_user.id,
              'email' => expected_user.email,
              'name' => expected_user.name,
              'avatar_url' => expected_user.avatar_url(:medium),
            },
            'exp' => Time.current.to_i + 2 * 3600,
          }

          JWT.encode(payload, subject_secret_key_base, subject_algorithm)
        }

        include_examples 'success method execution'
      end

      context 'pass jwt with valid secret and invalid payload user id' do
        let!(:subject_jwt) {
          payload = {
            'user' => {
              'id' => 'invalid_id',
              'email' => users.second.email,
              'name' => users.second.name,
              'avatar_url' => users.second.avatar_url(:medium),
            },
            'exp' => Time.current.to_i + 2 * 3600,
          }

          JWT.encode(payload, subject_secret_key_base, subject_algorithm)
        }

        include_examples 'failure method execution'
      end

      context 'pass jwt with valid secret and payload without user' do
        let!(:subject_jwt) {
          payload = {
            'exp' => Time.current.to_i + 2 * 3600,
          }

          JWT.encode(payload, subject_secret_key_base, subject_algorithm)
        }

        include_examples 'failure method execution'
      end

      context 'pass jwt with valid secret and payload without user id' do
        let!(:subject_jwt) {
          payload = {
            'user' => {
              'email' => users.second.email,
              'name' => users.second.name,
              'avatar_url' => users.second.avatar_url(:medium),
            },
            'exp' => Time.current.to_i + 2 * 3600,
          }

          JWT.encode(payload, subject_secret_key_base, subject_algorithm)
        }

        include_examples 'failure method execution'
      end

      context 'pass jwt with invalid secret and valid payload' do
        let!(:subject_jwt) {
          payload = {
            'user' => {
              'id' => users.second.id,
              'email' => users.second.email,
              'name' => users.second.name,
              'avatar_url' => users.second.avatar_url(:medium),
            },
            'exp' => Time.current.to_i + 2 * 3600,
          }

          <<~TEXT.gsub("\n", '')
            eyJhbGciOiJIUzI1NiJ9.
            #{Base64.encode64(payload.to_s)}.
            GdeovlFRgXCdn8rpf_s4f5ic25j6Szy-cJfRaqci-LH0
          TEXT
        }

        include_examples 'failure method execution'
      end

      context 'pass jwt with expired secret and valid payload' do
        let!(:subject_jwt) {
          payload = {
            'user' => {
              'id' => users.second.id,
              'email' => users.second.email,
              'name' => users.second.name,
              'avatar_url' => users.second.avatar_url(:medium),
            },
            'exp' => Time.current.to_i - 1,
          }

          JWT.encode(payload, subject_secret_key_base, subject_algorithm)
        }

        include_examples 'failure method execution'
      end

      context 'pass jwt with invalid format' do
        let!(:subject_jwt) {
          <<~TEXT.squish
            e1yJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7ImlkIjoiYTRkOGU5N2UtZjk2Ni00NzRkLThjYjAtMzEyNT
            hhZWU0ZjIwIiwiZW1haWwiOiJuZXdfdXNlckBleGFtcGxlLmNvbSIsIm5hbWUiOiJOZXcgTmFtZSIsIm
            F2YXRhcl91cmwiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAvaW1hZ2VzL2RlZmF1bHRfdXNlcl9tZWRpdW
            0uanBnIn0sImV4cCI6MTY0OTYyMTc5N30.pX3JI-BwyLZs84wOUUKTDAMaqo1THdT-KIuLpsf2DQo
          TEXT
        }

        include_examples 'failure method execution'
      end
    end
  end
end
