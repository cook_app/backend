require 'rails_helper'

RSpec.describe Oauth::GoogleAuthService, type: :service do
  describe '#call' do
    subject {
      described_class.
        new(validator: validator, oauth_google_ios_client_id: oauth_google_ios_client_id).
        call(params: params)
    }

    shared_examples 'success method execution' do
      it '- returns correct result object' do
        subject_result = subject

        aggregate_failures('check data returned from the method') do
          expect(subject_result.success?).to(be(true))
          expect(subject_result.data[:user]&.persisted?).to(be(true))
          expect(subject_result.data[:user]&.email).to(eq(expected_user_email))
          expect(subject_result.data[:user]&.name).to(eq(expected_user_name))
          expect(subject_result.data[:user]&.avatar_url.present?).to(be(true))
        end

        aggregate_failures('check DB data') do
          expect(User.all.count).to(eq(expected_users_count))
          expect(Oauth::Identifier.all.count).to(eq(expected_oauth_identifiers_count))
        end
      end
    end

    shared_examples 'failure method execution' do
      it '- returns failure result object' do
        subject_result = subject

        aggregate_failures('check data returned from the method') do
          expect(subject_result.failure?).to(be(true))
          expect(subject_result.data[:error].present?).to(be(true))
          expect(subject_result.data.keys.count).to(eq(1))
        end

        aggregate_failures('check DB data') do
          expect(User.all.count).to(eq(expected_users_count))
          expect(Oauth::Identifier.all.count).to(eq(expected_oauth_identifiers_count))
        end
      end
    end

    before do
      stub_request(:get, 'https://lh3.googleusercontent.com/a-/AOh14GiyUbkARHltiVTUXEvw_5ABWrkAhol5vu0I_HEifQ=s96-c.jpg')
        .to_return(
          :status => 200,
          :body => File.read('spec/data/mock_image_response.jpg'),
          :headers => {}
        )
    end

    let!(:validator) {
      Class.new do
        def valid_token
          <<~TEXT.squish
            eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
            eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiJjbGllbnRfaWQiLCJhdWQiOiJjbGllbnRfaWQiLCJzdW
            IiOiIxMDQzMDMwMjkzNTc0NTgzODY0NzciLCJlbWFpbCI6ImV4aXN0QGV4YW1wbGUuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUs
            ImF0X2hhc2giOiJraVd2MHpQbDhSaC10WGtRUjZTUnJnIiwibmFtZSI6ItCY0LzRjyDQpNCw0LzQuNC70LjRjyIsInBpY3R1cmUiOi
            JodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS0vQU9oMTRHaXlVYmtBUkhsdGlWVFVYRXZ3XzVBQldya0Fob2w1dnUw
            SV9IRWlmUT1zOTYtYyIsImdpdmVuX25hbWUiOiLQmNC80Y8iLCJmYW1pbHlfbmFtZSI6ItCk0LDQvNC40LvQuNGPIiwibG9jYWxlIj
            oicnUiLCJpYXQiOjE2NDk1MjM2ODgsImV4cCI6MTY0OTUyNzI4OH0.
            VVgArqYNFNpCSnoRBlEz5HuONRaiLIo3eYWPmpTMJ5A
          TEXT
        end

        def valid_client_id
          'valid_client_id'
        end

        def success_payload
          {
            "iss" => "https://accounts.google.com",
            "azp" => "client_id",
            "aud" => "client_id",
            "sub" => "104303029357458386477",
            "email" => "exist@example.com",
            "email_verified" => true,
            "at_hash" => "kiWv0zPl8Rh-tXkQR6SRrg",
            "name" => "Имя Фамилия",
            "picture" => "https://lh3.googleusercontent.com/a-/AOh14GiyUbkARHltiVTUXEvw_5ABWrkAhol5vu0I_HEifQ=s96-c.jpg",
            "given_name" => "Имя",
            "family_name" => "Фамилия",
            "locale" => "ru",
            "iat" => 1649523688,
            "exp" => 1649527288,
          }
        end

        def check(token, client_id)
          if token == valid_token && client_id == valid_client_id
            success_payload
          else
            raise GoogleIDToken::ValidationError
          end
        end
      end.new
    }

    context 'pass valid oauth_google_ios_client_id' do
      let!(:oauth_google_ios_client_id) {
        validator.valid_client_id
      }

      context 'and pass valid token in hash format' do
        let!(:params) {
          {
            'token' => validator.valid_token,
          }
        }
        let!(:expected_user_email) { validator.success_payload['email'] }
        let!(:expected_user_name) { validator.success_payload['name'] }
        let!(:expected_users_count) { 1 }
        let!(:expected_oauth_identifiers_count) { 1 }

        include_examples 'success method execution'
      end

      context 'and pass valid token in action controller params format' do
        let!(:params) {
          ActionController::Parameters.new(
            token: validator.valid_token,
          )
        }
        let!(:expected_user_email) { validator.success_payload['email'] }
        let!(:expected_user_name) { validator.success_payload['name'] }
        let!(:expected_users_count) { 1 }
        let!(:expected_oauth_identifiers_count) { 1 }

        include_examples 'success method execution'
      end

      context 'and pass invalid token in action controller params format' do
        let!(:params) {
          ActionController::Parameters.new(
            token: 'invalid_client_token',
          )
        }
        let!(:expected_users_count) { 0 }
        let!(:expected_oauth_identifiers_count) { 0 }

        include_examples 'failure method execution'
      end
    end

    context 'pass invalid client id' do
      let!(:oauth_google_ios_client_id) {
        'invalid_client_id'
      }

      context 'and pass valid token in hash format' do
        let!(:params) {
          {
            'token' => validator.valid_token,
          }
        }
        let!(:expected_users_count) { 0 }
        let!(:expected_oauth_identifiers_count) { 0 }

        include_examples 'failure method execution'
      end

      context 'and pass valid token in action controller params format' do
        let!(:params) {
          ActionController::Parameters.new(
            token: validator.valid_token,
          )
        }
        let!(:expected_users_count) { 0 }
        let!(:expected_oauth_identifiers_count) { 0 }

        include_examples 'failure method execution'
      end

      context 'and pass invalid token in action controller params format' do
        let!(:params) {
          ActionController::Parameters.new(
            token: 'invalid_client_token',
          )
        }
        let!(:expected_users_count) { 0 }
        let!(:expected_oauth_identifiers_count) { 0 }

        include_examples 'failure method execution'
      end
    end
  end
end
