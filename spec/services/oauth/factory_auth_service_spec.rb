require 'rails_helper'

RSpec.describe Oauth::FactoryAuthService, type: :service do
  describe '#call' do
    subject { described_class.new(env: env).call(provider: provider) }

    shared_examples 'success method execution' do
      it '- returns correct auth service' do
        subject_result = subject

        aggregate_failures('check data returned from the method') do
          expect(subject_result.success?).to(be(true))
          expect(subject_result.data[:auth_service].present?).to(be(true))
          expect(subject_result.data[:auth_service].class).to(eq(expected_auth_service_class))
        end
      end
    end

    shared_examples 'failure method execution' do
      it '- returns failure result object' do
        subject_result = subject

        aggregate_failures('check data returned from the method') do
          expect(subject_result.failure?).to(be(true))
          expect(subject_result.data[:error].present?).to(be(true))
          expect(subject_result.data.keys.count).to(eq(1))
        end
      end
    end

    context 'env is development' do
      let!(:env) { 'development' }

      context 'pass developer provider' do
        let!(:provider) { 'developer' }
        let!(:expected_auth_service_class) { ::Oauth::DeveloperAuthService }

        include_examples 'success method execution'
      end

      context 'pass google provider' do
        let!(:provider) { 'google' }
        let!(:expected_auth_service_class) { ::Oauth::GoogleAuthService }

        include_examples 'success method execution'
      end

      context 'pass unknown provider' do
        let!(:provider) { 'unknown' }

        include_examples 'failure method execution'
      end
    end

    context 'env is production' do
      let!(:env) { 'production' }

      context 'pass developer provider' do
        let!(:provider) { 'developer' }

        include_examples 'failure method execution'
      end

      context 'pass google provider' do
        let!(:provider) { 'google' }
        let!(:expected_auth_service_class) { ::Oauth::GoogleAuthService }

        include_examples 'success method execution'
      end

      context 'pass unknown provider' do
        let!(:provider) { 'unknown' }

        include_examples 'failure method execution'
      end
    end
  end
end
