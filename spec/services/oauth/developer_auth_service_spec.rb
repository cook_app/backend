require 'rails_helper'

RSpec.describe Oauth::DeveloperAuthService, type: :service do
  describe '#call' do
    subject { described_class.new.call(params: params) }

    shared_examples 'success method execution' do
      it '- returns correct result object' do
        subject_result = subject

        aggregate_failures('check data returned from the method') do
          expect(subject_result.success?).to(be(true))
          expect(subject_result.data[:user]&.persisted?).to(be(true))
          expect(subject_result.data[:user]&.email).to(eq(expected_user_email))
          expect(subject_result.data[:user]&.name).to(eq(expected_user_name))
        end

        aggregate_failures('check DB data') do
          expect(User.all.count).to(eq(expected_users_count))
          expect(Oauth::Identifier.all.count).to(eq(expected_oauth_identifiers_count))
        end
      end
    end

    shared_examples 'failure method execution' do
      it '- returns failure result object' do
        subject_result = subject

        aggregate_failures('check data returned from the method') do
          expect(subject_result.failure?).to(be(true))
          expect(subject_result.data[:error].present?).to(be(true))
          expect(subject_result.data.keys.count).to(eq(1))
        end

        aggregate_failures('check DB data') do
          expect(User.all.count).to(eq(expected_users_count))
          expect(Oauth::Identifier.all.count).to(eq(expected_oauth_identifiers_count))
        end
      end
    end

    context 'has one user' do
      let!(:user) { create(:user, email: 'exist@example.com', name: 'exist') }

      context 'pass data of new user in hash format' do
        let!(:params) {
          {
            'provider_user_id' => '111222',
            'email' => 'new@example.com',
            'name' => 'new',
          }
        }
        let!(:expected_user_email) { 'new@example.com' }
        let!(:expected_user_name) { 'new' }
        let!(:expected_users_count) { 2 }
        let!(:expected_oauth_identifiers_count) { 1 }

        include_examples 'success method execution'
      end

      context 'and pass data of new user in action controller params format' do
        let!(:params) {
          ActionController::Parameters.new(
            provider_user_id: '111222',
            email: 'new@example.com',
            name: 'new',
          )
        }
        let!(:expected_user_email) { 'new@example.com' }
        let!(:expected_user_name) { 'new' }
        let!(:expected_users_count) { 2 }
        let!(:expected_oauth_identifiers_count) { 1 }

        include_examples 'success method execution'
      end

      context 'and pass data of exist user in hash format but with different name' do
        let!(:params) {
          {
            'provider_user_id' => '111222',
            'email' => user.email,
            'name' => 'new',
          }
        }
        let!(:expected_user_email) { user.email }
        let!(:expected_user_name) { user.name }
        let!(:expected_users_count) { 1 }
        let!(:expected_oauth_identifiers_count) { 1 }

        include_examples 'success method execution'
      end

      context 'and pass blank params in hash format' do
        let!(:params) {
          {
          }
        }
        let!(:expected_users_count) { 1 }
        let!(:expected_oauth_identifiers_count) { 0 }

        include_examples 'failure method execution'
      end

      context 'and pass blank params in action controller params format' do
        let!(:params) {
          ActionController::Parameters.new
        }
        let!(:expected_users_count) { 1 }
        let!(:expected_oauth_identifiers_count) { 0 }

        include_examples 'failure method execution'
      end

      context 'and user has developer identifier' do
        let!(:oauth_identifier) {
          create(:oauth_identifier, provider: 'developer', provider_user_id: '111222', user: user)
        }

        context 'and pass data of exist user and oauth identifier but different user data' do
          let!(:params) {
            {
              'provider_user_id' => oauth_identifier.provider_user_id,
              'email' => 'new@example.com',
              'name' => 'new',
            }
          }
          let!(:expected_user_email) { user.email }
          let!(:expected_user_name) { user.name }
          let!(:expected_users_count) { 1 }
          let!(:expected_oauth_identifiers_count) { 1 }

          include_examples 'success method execution'
        end

        context 'and pass data of exist user but different provider_user_id' do
          let!(:params) {
            {
              'provider_user_id' => '0000',
              'email' => user.email,
              'name' => user.name,
            }
          }
          let!(:expected_users_count) { 1 }
          let!(:expected_oauth_identifiers_count) { 1 }

          include_examples 'failure method execution'
        end
      end
    end
  end
end
