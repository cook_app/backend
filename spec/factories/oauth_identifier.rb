require 'faker'

FactoryBot.define do
  factory :oauth_identifier, class: 'Oauth::Identifier' do
    user

    provider { 'google' }
    provider_user_id { SecureRandom.hex(10) }
  end
end
