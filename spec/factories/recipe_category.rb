require 'faker'

FactoryBot.define do
  factory :recipe_category, class: 'Recipe::Category' do
    name { Faker::Food.dish }
  end
end
