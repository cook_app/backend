require 'faker'

FactoryBot.define do
  factory :ingredient do
    ingredient_category

    name { Faker::Food.dish }
    description { Faker::Food.description }
    unit { Ingredient.units.keys.sample }
  end
end
