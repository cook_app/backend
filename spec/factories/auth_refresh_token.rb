require 'faker'

FactoryBot.define do
  factory :auth_refresh_token, class: 'Auth::RefreshToken' do
    transient do
      refresh_token { SecureRandom.hex(32) }
    end

    user

    encrypted_refresh_token { BCrypt::Password.create(refresh_token) }
    expires_at { Time.current + 1.day }
  end
end
