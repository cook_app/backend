require 'faker'

FactoryBot.define do
  factory :recipe_ingredient, class: 'Recipe::Ingredient' do
    recipe
    ingredient

    amount { Faker::Number.number(digits: 3) }
  end
end
