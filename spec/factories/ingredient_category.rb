require 'faker'

FactoryBot.define do
  factory :ingredient_category, class: 'Ingredient::Category' do
    name { Faker::Food.dish }
  end
end
