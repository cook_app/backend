require 'faker'

FactoryBot.define do
  factory :recipe do
    recipe_category

    name { Faker::Food.dish }
    description { Faker::Food.description }
    kilocalories { Faker::Number.number(digits: 3) }
    fats { Faker::Number.number(digits: 3) }
    carbohydrates { Faker::Number.number(digits: 3) }
    proteins { Faker::Number.number(digits: 3) }
  end
end
