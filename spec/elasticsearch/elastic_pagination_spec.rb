require 'rails_helper'

RSpec.describe ElasticPagination, type: :elasticsearch do
  describe '#build_query' do
    subject { described_class.new(page: page, size: size).build_query(query: query) }

    shared_examples 'success method execution' do
      it '- returns correct query' do
        query_with_pagination = subject

        aggregate_failures('check data returned from the method') do
          expect(query_with_pagination).to(eq(expected_query_with_pagination))
        end
      end
    end

    context 'pass base query' do
      let!(:query) {
        {
          query: {
            match_all: {},
          },
        }
      }

      context 'and page is nil, size is nil' do
        let!(:page) { nil }
        let!(:size) { nil }
        let!(:expected_query_with_pagination) {
          {
            **query,
            from: 0,
            size: ELASTICSEARCH_DEFAULT_SIZE,
          }
        }
  
        include_examples 'success method execution'
      end

      context 'and page is 0, size is 10' do
        let!(:page) { 0 }
        let!(:size) { 10 }
        let!(:expected_query_with_pagination) {
          {
            **query,
            from: 0,
            size: size,
          }
        }
  
        include_examples 'success method execution'
      end

      context 'and page is 1, size is 10' do
        let!(:page) { 1 }
        let!(:size) { 10 }
        let!(:expected_query_with_pagination) {
          {
            **query,
            from: 0,
            size: size,
          }
        }
  
        include_examples 'success method execution'
      end

      context 'and page is -1, size is 10' do
        let!(:page) { -1 }
        let!(:size) { 10 }
        let!(:expected_query_with_pagination) {
          {
            **query,
            from: 0,
            size: size,
          }
        }
  
        include_examples 'success method execution'
      end

      context 'and page is 5, size is 15' do
        let!(:page) { 5 }
        let!(:size) { 15 }
        let!(:expected_query_with_pagination) {
          {
            **query,
            from: 60,
            size: size,
          }
        }
  
        include_examples 'success method execution'
      end

      context 'and page is 5, size is -15' do
        let!(:page) { 5 }
        let!(:size) { -15 }
        let!(:expected_query_with_pagination) {
          {
            **query,
            from: 60,
            size: 15,
          }
        }
  
        include_examples 'success method execution'
      end

      context 'and page is 5, size is 0' do
        let!(:page) { 5 }
        let!(:size) { 0 }
        let!(:expected_query_with_pagination) {
          {
            **query,
            from: 0,
            size: 0,
          }
        }
  
        include_examples 'success method execution'
      end
    end
  end

  describe '#pagination_options' do
    subject { described_class.new(page: page, size: size).pagination_options(query_response: query_response) }

    shared_examples 'success method execution' do
      it '- returns correct query' do
        pagination_options = subject

        aggregate_failures('check data returned from the method') do
          expect(pagination_options).to(eq(expected_pagination_options))
        end
      end
    end

    context 'total hits value is 0, page is 1, size is 10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 0
            },
          },
        }
      }
      let!(:page) { 1 }
      let!(:size) { 10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 0,
          prev_page: nil,
          next_page: nil,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 21, page is 1, size is 10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 21
            },
          },
        }
      }
      let!(:page) { 1 }
      let!(:size) { 10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 3,
          prev_page: nil,
          next_page: 2,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 10, page is 1, size is 10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 10
            },
          },
        }
      }
      let!(:page) { 1 }
      let!(:size) { 10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 1,
          prev_page: nil,
          next_page: nil,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 9, page is 1, size is 10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 9
            },
          },
        }
      }
      let!(:page) { 1 }
      let!(:size) { 10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 1,
          prev_page: nil,
          next_page: nil,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 1, page is 1, size is 10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 1
            },
          },
        }
      }
      let!(:page) { 1 }
      let!(:size) { 10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 1,
          prev_page: nil,
          next_page: nil,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 15, page is 2, size is 10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 15
            },
          },
        }
      }
      let!(:page) { 2 }
      let!(:size) { 10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 2,
          prev_page: 1,
          next_page: nil,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 29, page is 2, size is 10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 29
            },
          },
        }
      }
      let!(:page) { 2 }
      let!(:size) { 10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 3,
          prev_page: 1,
          next_page: 3,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 29, page is 2, size is 0' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 29
            },
          },
        }
      }
      let!(:page) { 2 }
      let!(:size) { 0 }
      let!(:expected_pagination_options) {
        {
          total_pages: 0,
          prev_page: nil,
          next_page: nil,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 29, page is 2, size is -10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 29
            },
          },
        }
      }
      let!(:page) { 2 }
      let!(:size) { -10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 3,
          prev_page: 1,
          next_page: 3,
        }
      }

      include_examples 'success method execution'
    end

    context 'total hits value is 29, page is -2, size is 10' do
      let!(:query_response) {
        {
          'hits' => {
            'total' => {
              'value' => 29
            },
          },
        }
      }
      let!(:page) { 1 }
      let!(:size) { 10 }
      let!(:expected_pagination_options) {
        {
          total_pages: 3,
          prev_page: nil,
          next_page: 2,
        }
      }

      include_examples 'success method execution'
    end
  end
end
