require 'rails_helper'

RSpec.describe ElasticQueryBuilders::RecipeQueryBuilder, type: :elasticsearch do
  describe '#call' do
    subject { described_class.new.build(**params) }

    shared_examples 'success method execution' do
      it '- returns correct query' do
        query = subject

        aggregate_failures('check data returned from the method') do
          expect(query).to(eq(expected_query))
        end
      end
    end

    context 'params object is blank' do
      let!(:params) {
        {}
      }
      let!(:expected_query) {
        {
          query: {
            match_all: {},
          },
        }
      }

      include_examples 'success method execution'
    end

    context 'params object has only full_text_value' do
      let!(:params) {
        {
          full_text_value: 'Макароны',
        }
      }
      let!(:expected_query) {
        {
          min_score: 0.5,
          query: {
            bool: {
              should: [
                {
                  multi_match: {
                    query: params[:full_text_value],
                    fields: %w[name^2 description^1.5],
                  },
                },
                {
                  nested: {
                    path: 'ingredients',
                    query: {
                      multi_match: {
                        query: params[:full_text_value],
                        fields: %w[ingredients.name ingredients.description],
                      },
                    },
                  },
                },
              ],
            },
          },
        }
      }

      include_examples 'success method execution'
    end

    context 'params object has only ingredient_ids_include' do
      let!(:params) {
        {
          ingredient_ids_include: [1, 2],
        }
      }
      let!(:expected_query) {
        {
          min_score: 0.5,
          query: {
            bool: {
              filter: [
                {
                  terms_set: {
                    'ingredients_ids': {
                      terms: params[:ingredient_ids_include],
                      minimum_should_match_script: {
                        source: 'params.num_terms',
                      },
                    },
                  },
                },
              ],
              should: {
                match_all: {},
              },
            },
          },
          sort: [
            'ingredients_count',
            '_score',
          ],
        }
      }

      include_examples 'success method execution'
    end

    context 'params object has only ingredient_ids_exclude' do
      let!(:params) {
        {
          ingredient_ids_exclude: [1, 2],
        }
      }
      let!(:expected_query) {
        {
          min_score: 0.5,
          query: {
            bool: {
              must_not: {
                nested: {
                  path: 'ingredients',
                  query: {
                    terms: {
                      'ingredients.id': params[:ingredient_ids_exclude],
                    },
                  },
                },
              },
              should: {
                match_all: {},
              },
            },
          },
        }
      }

      include_examples 'success method execution'
    end

    context 'params object has only tool_types' do
      let!(:params) {
        {
          tool_types: ['knife'],
        }
      }
      let!(:expected_query) {
        {
          min_score: 0.5,
          query: {
            bool: {
              filter: [
                {
                  terms_set: {
                    'tool_types': {
                      terms: params[:tool_types],
                      minimum_should_match_script: {
                        source: 'params.num_terms',
                      },
                    },
                  },
                },
              ],
              should: {
                match_all: {},
              },
            },
          },
          sort: [
            'tool_types_count',
            '_score',
          ],
        }
      }

      include_examples 'success method execution'
    end

    context 'params object contains all available values' do
      let!(:params) {
        {
          full_text_value: 'Макароны',
          ingredient_ids_include: [1, 2],
          ingredient_ids_exclude: [3, 4],
          tool_types: ['knife'],
        }
      }
      let!(:expected_query) {
        {
          min_score: 0.5,
          query: {
            bool: {
              filter: [
                {
                  terms_set: {
                    'ingredients_ids': {
                      terms: params[:ingredient_ids_include],
                      minimum_should_match_script: {
                        source: 'params.num_terms',
                      },
                    },
                  },
                },
                {
                  terms_set: {
                    'tool_types': {
                      terms: params[:tool_types],
                      minimum_should_match_script: {
                        source: 'params.num_terms',
                      },
                    },
                  },
                },
              ],
              must_not: {
                nested: {
                  path: 'ingredients',
                  query: {
                    terms: {
                      'ingredients.id': params[:ingredient_ids_exclude],
                    },
                  },
                },
              },
              should: [
                {
                  multi_match: {
                    query: params[:full_text_value],
                    fields: %w[name^2 description^1.5],
                  },
                },
                {
                  nested: {
                    path: 'ingredients',
                    query: {
                      multi_match: {
                        query: params[:full_text_value],
                        fields: %w[ingredients.name ingredients.description],
                      },
                    },
                  },
                },
              ],
            },
          },
          sort: [
            'ingredients_count',
            'tool_types_count',
            '_score',
          ],
        }
      }

      include_examples 'success method execution'
    end
  end
end
