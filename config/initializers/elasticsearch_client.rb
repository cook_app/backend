Elasticsearch::Model.client = if Rails.env.production?
  Elasticsearch::Client.new(url: Rails.application.credentials.dig(:production, :elasticsearch_url), log: true)
else
  Elasticsearch::Client.new(url: ENV['ELASTICSEARCH_URL'], log: true)
end
