require 'sidekiq-limit_fetch'

redis_url = nil
redis_password = nil

if Rails.env.production?
  redis_url = Rails.application.credentials.dig(:production, :redis_url)
  redis_password = Rails.application.credentials.dig(:production, :redis_password)
else
  redis_url = ENV['REDIS_URL']
  redis_password = ENV['REDIS_PASSWORD']
end

Sidekiq.configure_server do |config|
  config.redis = {
    url: redis_url,
    password: redis_password,
    namespace: :sidekiq,
  }
end

Sidekiq.configure_client do |config|
  config.redis = {
    url: redis_url,
    password: redis_password,
    namespace: :sidekiq,
  }
end
