require 'shrine'
require 'shrine/storage/file_system'

if Rails.env.production?
  require 'shrine/storage/s3'

  s3_options = {
    endpoint: Rails.application.credentials.dig(Rails.env.to_sym, :s3_endpoint),
    bucket: Rails.application.credentials.dig(Rails.env.to_sym, :s3_bucket),
    region: Rails.application.credentials.dig(Rails.env.to_sym, :s3_region),
    access_key_id: Rails.application.credentials.dig(Rails.env.to_sym, :s3_access_key_id),
    secret_access_key: Rails.application.credentials.dig(Rails.env.to_sym, :s3_secret_access_key),
  }

  Shrine.storages = {
    # Temporary
    cache: Shrine::Storage::S3.new(public: true, prefix: 'uploads/cache', **s3_options),
    # Permanent
    store: Shrine::Storage::S3.new(public: true, prefix: 'uploads', **s3_options),
  }
else
  Shrine.storages = {
    # Temporary
    cache: Shrine::Storage::FileSystem.new('public', prefix: 'uploads/cache'),
    # Permanent
    store: Shrine::Storage::FileSystem.new('public', prefix: 'uploads'),
  }
end

# Loads Active Record integration
Shrine.plugin :activerecord
# Enables retaining cached file across form redisplays
Shrine.plugin :cached_attachment_data
# Extracts metadata for assigned cached files
Shrine.plugin :restore_cached_data
Shrine.plugin :validation
Shrine.plugin :validation_helpers
Shrine.plugin :derivatives, create_on_promote: true
Shrine.plugin :default_url, host: Rails.application.routes.default_url_options[:host]
Shrine.plugin :determine_mime_type, analyzer: :fastimage
Shrine.plugin :remote_url, max_size: 20*1024*1024
