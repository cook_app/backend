require 'sidekiq/web'
require 'sidekiq-scheduler/web'

if Rails.env.production?
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    is_correct_username = username == Rails.application.credentials.dig(:production, :sidekiq_web_username)
    is_correct_password = password == Rails.application.credentials.dig(:production, :sidekiq_web_password)

    is_correct_username && is_correct_password
  end
else
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == ENV['SIDEKIQ_WEB_USERNAME'] && password == ENV['SIDEKIQ_WEB_PASSWORD']
  end
end

Rails.application.routes.draw do
  ActiveAdmin.routes(self)

  mount Sidekiq::Web => '/sidekiq'

  namespace :api do
    namespace :v1 do
      resources :recipes, only: %i[index show]

      resources :ingredients, only: :show do
        get :autocomplete_search, on: :collection
      end

      resources :auth, only: [] do
        post :refresh_token_pair, on: :collection
      end

      resources :favorite_recipes, only: [] do
        post :create_or_destroy, on: :collection
      end

      resources :tools, only: :index

      resources :marks, only: :destroy do
        post :create_or_update, on: :collection
      end
    end
  end

  post '/oauth/:provider/auth', to: 'oauth#auth', as: 'oauth_auth'
end
