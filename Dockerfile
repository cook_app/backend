FROM ruby:3.0.2-alpine3.14

ENV APP_PATH /app

WORKDIR $APP_PATH

# ===== Install dependencies =====

# --- Main dependencies ---

# - build-base: To ensure certain gems can be compiled
# - nodejs yarn: Compile assets
# - postgresql-dev postgresql-client: Communicate with postgres through the postgres gem
# - imagemagick: for image processing
# - vim: for editing files inside a container
RUN apk --update add build-base nodejs yarn tzdata postgresql-dev postgresql-client \
                     imagemagick vim

# ===== Install gems =====

COPY Gemfile ./Gemfile
COPY Gemfile.lock ./Gemfile.lock

# Версия должна быть такой-же, что и в Gemfile.lock
RUN gem install bundler:2.2.30

RUN bundle install

# ===== Copy code =====

COPY . .

# ===== Precompile assets (js, css, images) =====

RUN RAILS_ENV=production rake assets:precompile

# ===== Expose port =====

EXPOSE 3000

# ===== Command =====

CMD ["rails", "server", "-b", "0.0.0.0"]
